Espera-se que a distribuição lateral de múons (Lateral Distribution Function, LDF) seja sensível à composição química do UHECR, visto que os múons se originam a partir do decaimento dos píons e káons e que partículas primárias de naturezas químicas diferentes sofrem processos hadrônicos diferentes. Para observar essas influências dos modelos de interações hadrônicas e da composição química, analisamos a distribuição lateral dos múons no nível do solo do Observatório Pierre Auger (1400 m acima do nível do mar), para os três principais modelos utilizados em simulações de EAS. Encontramos o perfil lateral da densidade de múons, caracterizamos a densidade de múons a 1000 m do ponto de impacto do chuveiro ($\rho_{1000}$) e também quantificamos a diferença entre os modelos para o intervalo de energia de $10^{18.5}$ até $10^{19.5}$ eV. Na fase de {\it design} do Auger se determinou, a partir de simulações, que nas energias em torno de $10^{18}$ eV, a flutuação chuveiro-a-chuveiro é mínima em torno de 1000 m do eixo da cascata. Por isso, essa é a distância de referência usada para se estimar o ``tamanho'' de uma cascata medida pelo Auger. 
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/analysis_all/ldf-original-all-proton-e19_00-t60.pdf}
    \caption{Comparação entre os modelos EPOSLHC, SIBYLL2.3 e QGSJETII-04 sobre a distribuição lateral (LDF) média para 1000 chuveiros iniciados por próton com energia de $10^{19}$ eV e ângulo $\theta=60\degree$ e $\phi=0\degree$.}
    \label{fig:ldf_compare}
\end{figure}

Na figura \ref{fig:ldf_compare} é mostrado o perfil lateral da densidade de múons ($\rho(r_{core})$) em função da distância do ponto de impacto do chuveiro. Para esse perfil, foram simulados EAS iniciados por prótons com energias de $10^{19}$ eV e inclinação zenital de $60\degree$ e azimutal de $0\degree$. A linha de cor azul representa o modelo SIBYLL2.3, a linha vermelha o EPOSLHC e a linha verde o QGSJETII-04. Para os três modelos, o perfil da densidade de múons é semelhante, não apresentando nenhuma característica distinta, e $\rho_{1000}$ é da ordem de uma partícula por metro quadrado. Para poder apreciar a diferença entre os modelos, é melhor analisar a diferença relativa 
\begin{equation}
    \Delta \, \% = \frac{\rho_{1} - \rho_{2}}{\rho_{2}}.
    \label{eqn:relative_diff}
\end{equation}
Na figura \ref{fig:ldf_ratio} é mostrada a diferença relativa entre as previsões dos três modelos de interação hadrônica. Em azul, SIBYLL2.3 e EPOSLHC apresenta uma diferença de aproximamente $\sim 10\%$ próxima ao ponto de impacto e essa diferença diminui até que, em torno 1000 m, ela seja praticamente nula. A diferença entre SIBYLL2.3 e QGSJETII-04, em verde, próxima ao ponto de impacto é aproximadamente  $\sim 5\%$ e aumenta para $\sim 10\%$ rapidamente ao afastar-se do ponto de impacto. Por último, em vermelho, a diferença entre EPOSLHC e QGSJETII-04 é de $\sim 7\%$ para regiões perto do ponto de impacto e aumenta para $\sim 10\%$ para mais longe do centro.
\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/analysis_all/ldf_ratio-original-all-proton-e19_00-t60.pdf}
    \caption{Diferença entre os modelos EPOSLHC, SIBYLL2.3 e QGSJETII-04 sobre a distribuição lateral (LDF).}
    \label{fig:ldf_ratio}
\end{figure}
Outra análise realizada sobre a densidade lateral de múons, foi a distribuição de $\rho_{1000}$ chuveiro a chuveiro. Na figura \ref{fig:rho_mean_proton_e19_00} é mostrada a distribuição de $\rho_{1000}$ para 1000 chuveiros iniciados por prótons com energia de $10^{19}$ eV. O modelo SIBYLL2.3 apresenta a maior densidade média entre os modelos, $\langle \rho_{1000} \rangle = 1.49 \pm 0.25_{RMS}$, o modelo EPOSLHC preve um valor muito próximo do SIBYLL2.3 com $\langle \rho_{1000} \rangle = 1.47 \pm 0.24_{RMS}$ e, por último, o QGSJETII-04 difere dos outros dois com $\langle \rho_{1000} \rangle = 1.32 \pm 0.20_{RMS}$. 
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/analysis_all/rho-original-all-proton-e19_00-t60.pdf}
    \caption{Distribuição de $\rho_{1000}$ entre os modelos EPOSLHC, SIBYLL2.3 e QGSJETII-04 para 1000 chuveiros iniciados por próton com energia de $10^{19}$ eV e ângulo $\theta=60\degree$ e $\phi=0\degree$.}
    \label{fig:rho_mean_proton_e19_00}
\end{figure}
A principal observação que podemos tirar desse resultado é que o modelo QGSJETII-04 tem uma densidade lateral de múons relativamente diferente dos modelos EPOSLHC e SIBYLL2.3 para chuveiro com energia de $10^{19}$ eV. Além do valor médio, as flutuações chuveiro a chuveiro de $\rho_{1000}$ são diferentes. O modelo QGSJETII-04 apresenta uma distribuição simétrica em torno do valor médio, enquanto as distribuições para os outros dois modelos possuem caudas assimétricas. Essa característica observada para  o valor médio de $\rho_{1000}$ ocorre para todo o intervalo de energia estudado nesse trabalho, como mostrado na figura \ref{fig:rho_vs_energy}. Para o intervalo de energia de $10^{18.5}$ até $10^{19}$ eV, os modelos SIBYLL2.3 e EPOSLHC são relativamente próximos, enquanto o modelo QGSJETII-04 apresenta uma densidade de múons menor que os outros dois. A diferença dos modelos em função da energia é constante. Todas as figuras que foram usadas para construir a figura de evolução de $\rho_{1000}$ em função da energia encontram-se no apêndice.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{chapters/analysis/images/rho_mil-all-all-energy-t60.pdf}
    \caption{Evolução do $\langle \rho_{1000} \rangle $ em função da energia para os modelos EPOSLHC, SIBYLL2.3 e QGSJETII-04 para chuveiros com energia entre $10^{18.5}$ e  $10^{19.5}$ eV e ângulo $\theta=60\degree$ e $\phi=0\degree$. Ao todo, foram simulados 1000 chuveiros para cada ponto do gráfico.}
    \label{fig:rho_vs_energy}
\end{figure}
