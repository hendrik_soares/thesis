A medida do desenvolvimento longitudinal de EAS possibilita o estudo da composição química do UHECR que iniciou o EAS, principalmente através da determinação do $X_{max}$. Espera-se que o núcleo de massa $A$ tenha a primeira interação a uma profundidade $\langle X_0 \rangle$ com comprimento de interação menor que de um próton ($\lambda_A < \lambda_p$). Além disso, a flutuação do valor de $X_{max}$ é menor para os núcleos quando a comparamos com a flutuação do valor de  $X_{max}$ de prótons. Esse resultado pode ser mostrado a partir do modelo de Matthews para o desenvolvimento de EAS. A profundidade $X_{max}$ neste modelo é dada pela equação \ref{eqn:matthews_xmax}. Nela, podemos ver claramente que $X_{max}$ depende do comprimento de interação $\lambda_p$ e da energia da partícula inicial. Se aplicarmos o modelo de superposição, descrito na seção \ref{sec:superposition_model}, podemos obter a equação da profundidade máxima
\begin{equation}
  \langle X^{A}_{max} \rangle = \langle X^{p}_{max} (E'=E/A) \rangle = \langle C+D_p \ln (E/A) \rangle = \langle X^{p}_{max} \rangle - D_p \langle \ln A \rangle
  \label{eqn:x_max_composition}
\end{equation}
que demonstra explicitamente a relação de $X_{max}$ com a composição química do UHECR que iniciou a cascata.

Em vista disso, analisamos o desenvolvimento longitudinal de EAS utilizando simulações com diferentes modelos de interações hadrônicas. Nessa análise, estudamos o número de partículas (múons, fótons e elétrons) em função da profundidade $X$, dada em g/cm$^2$, mostrado na figura \ref{fig:depth_compare}. Na figura (a) é mostrado o perfil médio do desenvolvimento longitudinal de chuveiros iniciados por prótons com energia de $10^{19}$ eV usando os modelos de interação hadrônica SIBYLL2.3 (linha azul), EPOSLHC (linha vermelha) e QGSJETII-04(linha verde). Aqui, é mostrado que o perfil médio de desenvolvimento longitudinal difere ligeiramente entre os modelos, mas essa diferença não é clara nesse tipo de gráfico. Contudo, quando se observa a figura (b), fica evidenciado que no início do chuveiro, onde ocorrem as primeiras interações, a diferença é da ordem de 60\%, depois, chega a ser inexistente perto da região do máximo e volta a aumentar para $\sim$ 20\% para grandes profundidades. A diferença no início do chuveiro é claramente uma consequência do modelo de interação hadrônica, pois é nesse estágio que ocorrem as interações hadrônicas de altas energias. 
\begin{figure}[!htb]
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
      \includegraphics[width=\textwidth]{chapters/analysis/images/depth-all-proton-e19_00-t60.pdf}
      \caption{Perfil médio}
	 \label{fig:depth_compare-a}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
      \includegraphics[width=\textwidth]{chapters/analysis/images/ratio_xmax-all-proton-e19_00-t60.pdf}
      \caption{Diferença}
		\label{fig:depth_compare-b}
    \end{subfigure}
    \caption{Comparação do desenvolvimento longitudinal de chuveiros atmosféricos iniciados por próton com energia de $10^{19}$ eV e inclinação $\theta = 60\degree$. Na figura (a) estão representados os perfis médios de 1000 EAS para os modelos EPOSLHC (linha vermelha), SIBYLL2.3 (linha azul), QGSJETII-04 (linha verde) e na figura (b) é mostrada a diferença entre os modelos.}
    \label{fig:depth_compare}
\end{figure}
As medidas experimentais do desenvolvimento longitudinal de EAS no Observatório Pierre Auger são realizadas com os telescópios de fluorescência através da taxa de emissão fluorescente $dE/dX$ em função da profundidade atmosférica. O perfil dessa emissão pode ser descrito por meio da função de Gaisser-Hillas \cite{pierre-auger-observatory}, dada por
\begin{equation}
  f_{GH} = \left( \frac{dE}{dX} \right)_{max} \left( \frac{X-X_0}{X_{max}-X_{0}} \right)^{(X_{max}-X_{0})/\lambda} e^{(X_{max}-X)/\lambda},
  \label{eqn:gaisser_hillas_function}
\end{equation}
A profundidade do número máximo de partículas chuveiro ($X_{max}$) é outro observável interessante dado pelo perfil longitudinal de EAS. Assim como o perfil do EAS, o $X_{max}$ também é estimado através do ajuste da equação \ref{eqn:gaisser_hillas_function}. Na figura \ref{fig:xmax_mean_proton_e19_00} é mostrada a distribuição de $X_{max}$ para chuveiros iniciados por próton com energia de $10^{19}$ eV. Para o modelo SIBYLL2.3 foi encontrado $\langle X_{max} \rangle = 820 \pm 65_{RMS}$ g/cm$^2$, enquanto o EPOSLHC foi encontrado $\langle X_{max} \rangle = 800 \pm 56_{RMS}$ g/cm$^2$ e para o QGSJETII-04 $\langle X_{max} \rangle = 790 \pm 64_{RMS}$ g/cm$^2$.
\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/analysis_all/xmax-original-all-proton-e19_00-t60.pdf}
    \caption{Distribuição de $X^{\mu}_{max}$ entre os modelos EPOSLHC, e QGSJETII-04 para 1000 chuveiros iniciados por próton com energia de $10^{19}$ eV e ângulo $\theta=60\degree$ e $\phi=0\degree$.}
    \label{fig:xmax_mean_proton_e19_00}
\end{figure}
 Quando estudamos outra composição como ferro ou outros intervalos de energia, a diferença entre os modelos se mostrou constante. O modelo SIBYLL2.3 sempre atingiu a maior profundidade, independente da massa ou energia, e em seguida EPOSLHC e QGSJETII-04, como mostrado na figura \ref{fig:xmax_vs_energy}. Ou seja, o $\langle X_{max} \rangle$ é uma função monotonamente crescente da energia com uma taxa de elongação de $\sim 60$ g/cm$^2$ por década de energia para todos os modelos.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{chapters/analysis/images/xmax-all-all-energy-t60.pdf}
    \caption{Evolução do $X_{max}$ em função da energia para os modelos EPOSLHC, SIBYLL2.3 e QGSJETII-04 para chuveiros com energia entre $10^{18.5}$ e  $10^{19.5}$ eV e ângulo $\theta=60\degree$ e $\phi=0\degree$.}
    \label{fig:xmax_vs_energy}
\end{figure}
Como esperado, o desenvolvimento longitudinal para chuveiros iniciados por ferro e próton são diferentes e, além disso, os modelos de interações hadrônicas resultam em perfil médio e profundidades máximas diferentes.
