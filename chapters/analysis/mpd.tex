A função que descreve a distribuição de profundidades atmosféricas de produção de múons (Muon Production Depth, MPD) representa outro observável de EAS. Sabemos que os múons são criados a partir do decaimento principalmente de píons e káons, de forma que a MPD contém informação sobre a evolução da cascata. Também sabemos que raios cósmicos de natureza química distintas têm processos hadrônicos distintos, consequentemente, teríamos diferentes perfis longitudinais da MPD. Portanto, é natural pensar que a MPD seja sensível à composição química do primário do UHECR que iniciou a cascata.

A profundidade de produção de múons $X^{\mu}$ é obtida da integração da densidade atmosférica $\rho$ ao longo do eixo da cascata até a altura de origem do múon
\begin{equation}
    X^{\mu} = \int^{\infty}_{z} \rho(z')dz'.
    \label{eqn:muon_production_depth}
\end{equation}
A forma da distribuição depende do ponto de observação no solo e do ângulo zenital do EAS. Além disso, ela depende da distribuição de energia dos múons, pois isso dita o tempo de vida médio dessas partículas. Sobre a distribuição de $X^{\mu}$ é ajustado a função de Gaisser-Hillas \ref{eqn:gaisser_hillas_function}. A partir desse ajuste, podemos determinar o perfil e a profundidade de máxima produção de múons $X^{\mu}_{max}$.

Para permitir comparações com a literatura, aplicamos um corte na distância ao ponto de impacto da cascata. Apenas os múons com mais de 1700 m de distância são considerados na distribuição de profundidade de produção de múons. Esse é o melhor valor encontrado na referência \cite{PhysRevD.90.012012}, onde se mostrou que incertezas no tempo de chegada para os múons próximo ao ponto de impacto são grandes e por isso devem ser descartados.

Para a análise da MPD, simularam-se 1000 chuveiros com energia de $10^{19}$ eV iniciados por próton com inclinação zenital de $60\degree$ e azimutal $0\degree$. A partir de cada chuveiro foi extraída uma MPD e sobre ela ajustada a função Gaisser-Hillas como mostrado na figura \ref{fig:fit_mpd-original-eposlhc-proton-e19_00-t60-0}. Aqui, os pontos pretos mostram os dados da MPD para um chuveiro iniciado por próton com energia de $10^{19}$ eV utilizando o modelo EPOSLHC, enquanto que a linha em vermelho é o melhor ajuste por minimização pelo método máxima verossimilhança.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/fit_mpd/mpd_fit-original-eposlhc-proton-e19_00-t60-0.pdf}
    \caption{Distribuição de $X^{\mu}$ em função da profundidade atmosférica de um EAS iniciado por um próton com energia de $10^{19}$ eV. Sobre os dados foi aplicado um corte, $r_{cut} > 1700$ m, na distância ao ponto de impacto do chuveiro. Além disso, a linha vermelha representa o melhor ajuste da função Gaisser-Hillas através da minimização de máxima verossimilhança.}
    \label{fig:fit_mpd-original-eposlhc-proton-e19_00-t60-0}
\end{figure}


Na figura \ref{fig:mpd_depth_compare} é mostrado o perfil médio de desenvolvimento da produção de múons e a diferença entre os modelos estudados. Os modelos EPOSLHC (linha vermelha) e SIBYLL2.3 (linha azul) apresentam um perfil semelhante, enquanto o QGSJETII-04 (linha verde) tem uma produção menor. Nos primeiros estágios de desenvolvimento do chuveiro, o modelo QGSJETII-04 apresenta uma taxa de $\sim$ 70\% a 80\% maior de produção de múons em relação aos outros modelos. Porém, durante o desenvolvimento, a taxa de produção diminui e a diferença se inverte e o modelo QGSJETII-04 passa a produzir $\sim$ 40\% a menos múons.
\begin{figure}[htb]
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
      \includegraphics[width=\textwidth]{../../graphics/analysis_all/mpd-original-all-proton-e19_00-t60.pdf}
      \caption{Perfil médio}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
      \includegraphics[width=\textwidth]{../../graphics/analysis_all/mpd_ratio-original-all-proton-e19_00-t60.pdf}
      \caption{Diferença}
    \end{subfigure}
    \caption{Comparação da MPD de chuveiros atmosféricos iniciados por próton com energia de $10^{19,5}$ eV e inclinação $\theta = 60\degree$. Na figura (a) é mostrado o perfil médio de 1000 chuveiros e na figura (b) está representada a diferença em porcentagem entre os modelos.}
    \label{fig:mpd_depth_compare}
\end{figure}

Assim como no perfil longitudinal, podemos estudar a profundidade de máxima produção de múons $X_{max}^{\mu}$. Essa profundidade é obtida através do ajuste da equação \ref{eqn:gaisser_hillas_function}. Na figura \ref{fig:mpd_mean_proton_e19_00} é mostrada a distribuição de $X_{max}^{\mu}$ para chuveiros iniciados por próton com energia de $10^{19}$ eV. Podemos ver na figura o valor médio de $X_{max}^{\mu}$ para o modelo SIBYLL2.3 é $\langle X_{max}^{\mu} \rangle = 629 \pm 81_{RMS}$, para o EPOSLHC $\langle X_{max}^{\mu} \rangle =  625 \pm 81_{RMS}$ e QGSJETII-04 $\langle X_{max}^{\mu} \rangle =  591 \pm 81_{RMS}$. 

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/analysis_all/max-original-all-proton-e19_00-t60.pdf}
    \caption{Distribuição de $X^{\mu}_{max}$ entre os modelos EPOSLHC, SIBYLL2.3 e QGSJETII-04 para 1000 chuveiros iniciados por próton com energia de $10^{19}$ eV e ângulo $\theta=60\degree$ e $\phi=0\degree$.}
    \label{fig:mpd_mean_proton_e19_00}
\end{figure}

A evolução de $\langle X_{max}^{\mu} \rangle$ em função da energia é mostrada na figura \ref{fig:mpd_vs_energy}. Os dados da figura mostram que a evolução da MPD é constante para todos os modelos e cresce a uma taxa de $\sim$ 38 g/cm$^2$ por década de energia. Entre todos os observáveis que foram estudados nessa seção, a MPD foi a que apresentou a maior taxa de variação no valor médio. Também é importante notar que o $X^{\mu}_{max}$ é sistematicamente menor que $X_{max}$ da parte eletromagnética.
\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\textwidth]{chapters/analysis/images/mpd_max-all-all-energy-t60.pdf}
    \caption{Evolução do $X^{\mu}_{max}$ em função da energia para os modelos EPOSLHC, SIBYLL2.3 e QGSJETII-04 para chuveiros com energia entre $10^{18,5}$ e  $10^{19,5}$ eV e ângulo $\theta=60\degree$ e $\phi=0\degree$.}
    \label{fig:mpd_vs_energy}
\end{figure}
