Com a simulação do arranjo de detectores validada sobre um cenário controlado, aplicamos o mesmo processo e reconstruímos o observável $N_{19}$ de dados de EAS simulados pelo programa CORSIKA. Aplicamos esse processo sobre dados de EAS iniciados por próton ou ferro, com energia entre $10^{18.5}$ até $10^{19.5}$ eV e ângulos $\theta = 60\degree$ e $\phi=0\degree$.  Primeiro reconstruímos a direção de chegada dos EAS para que em seguida pudéssemos iniciar a reconstrução do parâmetro $N_{19}$, pois o mesmo depende da inclinação de chegada do EAS.

Inicialmente, realizou-se um estudo do observável $N_{19}$ para chuveiros iniciados por próton com energia de $10^{19}$ eV e ângulos zenital de $60\degree$ e azimutal $0\degree$. Sobre esse processo avaliamos a resolução angular e a precisão da reconstrução de $ \langle N_{19} \rangle $ usando EAS simulados com os três principais modelos de interações hadrônicas. Além disso, também estudamos o evolução do parâmetro reconstruído no intervalo de energia entre $10^{18.5}$ até $10^{19.5}$ eV.

\subsection{Reconstruindo a direção de chegada para dados de EAS}

Aplicamos o processo de reconstrução descrito na seção \ref{sec:direction_arrive} e utilizamos o modelo de frente de chuveiros com curvatura parabólica. Analisamos a distribuição angular de $\theta$ e $\phi$ reconstruídos, mostradas nas figuras \ref{fig:theta-original-all-proton-e19_00-t60}  e \ref{fig:phi-original-all-proton-e19_00-t60}. 
\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/model_detection/theta-original-all-proton-e19_00-t60.pdf}
    \caption{Reconstrução da distribuição de $\theta$ para os modelos de interação hadrônica SIBYLL2.3(azul), EPOSLHC(vermelho) e QGSJETII-04(verde). Foram reconstruídos 1000 EAS inciados por próton com energia de $10^{19}$ eV e inclinação $\theta=60\degree$ e $\phi=0\degree$.}
    \label{fig:theta-original-all-proton-e19_00-t60}
\end{figure}
\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/model_detection/phi-original-all-proton-e19_00-t60.pdf}
    \caption{Reconstrução da distribuição de $\phi$ para os modelos de interação hadrônica SIBYLL2.3(azul), EPOSLHC(vermelho) e QGSJETII-04(verde). Foram reconstruídos 1000 EAS inciados por próton com energia de $10^{19}$ eV e inclinação $\theta=60\degree$ e $\phi=0\degree$.}
    \label{fig:phi-original-all-proton-e19_00-t60}
\end{figure}
Todos os modelos apresentaram uma distribuição semelhante e valores médios iguais, como era esperado. Para uma amostra de 1000 EAS, encontramos o valor de $\langle \theta \rangle = 59.01 \pm 2.84_{RMS} \degree$ e $\langle \phi \rangle = -0.18 \pm 2.70_{RMS} \degree$ para o modelo EPOSLHC, em vermelho,  $\langle \theta \rangle = 59.20 \pm 2.62_{RMS} \degree$ e $\langle \phi \rangle = 0.10 \pm 2.64_{RMS} \degree$ para o modelo SIBYLL2.3, em azul, e $\langle \theta \rangle = 59.19 \pm 2.70_{RMS} \degree$ e $\langle \phi \rangle = 0.11 \pm 2.52_{RMS} \degree$ para o modelo QGSJETII-04, em verde.  Na média a reconstrução obteve uma incerteza sistemática de aproximadamente ~$1\degree$ para $\theta$ e ~$0.2\degree$ para $\phi$. Esse viés é natural, pois o processo de reconstrução pressupõe algumas hipóteses sobre a flutuação dos sinais nos detectores:
\begin{enumerate}
    \item O modelo de ``dethinning'' é válido e recupera corretamente a flutuação espacial e temporal das partículas;
    \item A propagação da frente do chuveiro tem uma curvatura parabólica;
    \item A flutuação no tempo de disparo de cada detector segue uma distribuição gaussiana  independente da distância ao eixo do chuveiro.
\end{enumerate}
A partir desses resultados, podemos calcular a resolução angular do processo de reconstrução utilizando a equação \ref{eqn:angular_resolution} onde obtemos $1.5\degree$ para SIBYLL2.3, $1.6\degree$ para EPOSLHC e $1.5\degree$ para QGSJETII-04.  Essa resolução angular é consistente com aquela observada na reconstrução geométrica de chuveiros a partir dos sinais nos detectores de superfície do Observatório Pierre Auger \cite{BONIFAZI200920}.

\subsection{Número de múons}

Uma vez reconstruída a direção de chegada ($\theta$ e $\phi$), podemos reconstruir o observável $N_{19}$ de EAS utilizando o processo de reconstrução descrito na seção \ref{sec:muon_number_rec}.  Na figura \ref{fig:n19-original-all-proton-e19_00-t60}, é mostrada a distribuição de $N_{19}$ onde obtivemos o valor médio de  $\langle N_{19} \rangle = 1.13 \pm 0.24_{RMS}$, $\langle N_{19} \rangle = 1.12 \pm 0.22_{RMS}$ e $\langle N_{19} \rangle = 1.02 \pm 0.19_{RMS}$ para SIBYLL2.3, EPOSLHC e QGSJETII-04 respectivamente.  
\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/model_detection/n19-original-all-proton-e19_00-t60.pdf}
    \caption{Reconstrução da distribuição de $N_{19}$ para os modelos de interação hadrônica SIBYLL2.3(azul), EPOSLHC(vermelho) e QGSJETII-04(verde).Foram reconstruídos 1000 EAS inciados por próton com energia de $10^{19}$ eV e inclinação $\theta=60\degree$ e $\phi=0\degree$.}
    \label{fig:n19-original-all-proton-e19_00-t60}
\end{figure}
Claramente, vemos que o processo de reconstrução de $N_{19}$ apresenta um viés de aproximadamente $0.1$ para todos os modelos. Esse desvio relativo na reconstrução de $N_{19}$, é conhecido e foi estudado em \cite{PhysRevD.91.032003}, onde é suposto que a maior fonte de incerteza pode estar associada ao perfil de densidade de referência ($\rho_{\mu,19}$). Para resolver esse problema, a Colaboração Pierre Auger construiu um estimador não enviesado para o número total de múons no solo que fosse independente do modelo e do ângulo de inclinação do EAS. Para esse estimador, usaram um polinômio de segunda ordem para corrigir $N_{19}$ reconstruído  com precisão de $3\%$ para todos os modelos. 

\subsection{Evolução em função da energia}

Ao final, é mostrada na \ref{fig:rec_n19_vs_energy}, a reconstrução do observável $N_{19}$ para chuveiros com energia entre $10^{18.5}$ e $10^{19.5}$ eV iniciados por raios cósmicos com composição química próton ou ferro. A diferença na reconstrução de $N_{19}$ é constante, independente do intervalo de energia estudado e, além disso, todas apresentaram um viés de aproximadamente 0.1. Assim, como na análise os modelos SIBYLL2.3 e EPOSLHC possuem um número de múons próximo. Já o QGSJETII-04 apresenta um número menor de múons em todo intervalo.
\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/reconstruct/n19-original-all-all-all-t60.pdf}
    \caption{Evolução da reconstrução do  número de múons para chuveiros com energia entre $10^{18.5}$ até $10^{19.5}$ eV e ângulo $\theta=60\degree$ e $\phi=0\degree$. Em vermelho é mostrado o resultado da reconstrução para o modelo interações hadrônicas EPOSLHC, em azul, SIBYLL2.3 e em verde, QGSJETII.}
    \label{fig:rec_n19_vs_energy}
\end{figure}
