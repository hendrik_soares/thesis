O número de partículas em um EAS é proporcional à energia do raio cósmico primário. Para energias acima de $10^{17}$ eV, torna-se inviável realizar a simulação completa, pois esta exige um alto custo computacional. O tempo de processamento vira um problema, pois pode chegar a dias ou meses para simular um chuveiro. Além disso, o armazenamento das partículas que chegam ao solo transforma-se em outro problema e exige grandes quantidades de memória.

\subsection{O método ``thinning''}

A solução para esse problema foi a aplicação de um método estatístico de diluição chamado ``thinning''. No caso de EAS, esse método é aplicado através do descarte de partículas e da aplicação de um peso estatístico às demais partículas que não foram descartadas. Por exemplo, no programa CORSIKA, todas as partículas secundárias que têm energia abaixo do nível de ``thinning'' ($\varepsilon_{th} = E/E_0$), serão expostas a esse procedimento. Ou seja, se a soma da energia de todas as $j$ partículas secundárias estiver abaixo da energia de ``thinning''
\begin{equation}
  \varepsilon_{th} E_0 > \sum_{j} E_{j},
\end{equation}
apenas uma das partículas secundárias será seguida. Ela será selecionada aleatoriamente de acordo com a sua energia $E_i$ com probabilidade 
\begin{equation}
  p_i = E_i / \sum_{j} E_j.
\end{equation}
e todas as outras serão descartadas. Um peso apropriado $w_i = 1/p_i$ é atribuído à partícula sobrevivente, a fim de conservar a energia. No caso de apenas algumas partículas secundárias terem energia abaixo do nível de ``thinning'', a partícula correspondente sobrevive com probabilidade $p_i$ dada por
\begin{equation}
  p_i = E_i / (\varepsilon_{th} E_0).
\end{equation}
e, no caso em que sobreviva, é atribuído  um peso estatístico  $w_i = 1/p_i$.

\subsection{Consequências em EAS}

Quando aplicamos o método de ``thinning'' no desenvolvimento dos EAS devemos ter cuidado com os dados produzidos pela simulação. A aplicação do método dilui a informação presente no chuveiro removendo partículas e atribuindo o valor dela a outras partículas. Para os detectores de fluorescência, o ``thinning'' não gera problemas significativos, porque o peso estatístico é muito menor que a densidade de partículas observada no detector. Por outro lado,  os detectores de superfície estão a quilômetros de distância do eixo do chuveiro onde a densidade de partícula é baixa, e chega a ser menor do que uma partícula/m$^2$.

Todas as simulações usadas nesse estudo utilizaram o nível de ``thinning'' $\varepsilon_{th} = 10^{-6}$. Dessa forma, cada múon produzido na simulação de EAS possui um peso estatístico médio alto de ~100. Ou seja, cada múon representa ao todo 100 outros múons. No final, para distâncias maiores do que 1000 m do ponto de impacto do chuveiro a distribuição de partículas se torna esparsa, como mostrado na figura \ref{fig:thinning_comparation_dethinning}. Logo, o processo de ``thinning'' está causando perda significativa na distribuição espacial dos múons que atingem o nível de observação para distâncias maiores do que 1000 m do ponto de impacto do chuveiro.

\subsection{Processo de ``dethinning''}

Uma vez determinada a causa do problema gerado pelo processo de ``thinning'', procuramos um método para recuperar a informação perdida. Na literatura existem dois métodos mais conhecidos para recriar o conjunto de partículas no nível de observação: (i) a partir das propriedades dos sinais gerados em detectores de superfície \cite{DethinningBilloir} e (ii) construir uma distribuição suavizada no espaço de parâmetros da partícula \cite{STOKES2012759}.  Ambos os métodos recuperam razoavelmente bem a distribuição de partículas no nível de observação. Porém nessa tese nos limitamos ao uso da opção (ii) conhecida também pelo nome de ``dethinning''. Esse modelo elimina os pesos estatísticos das partículas convertendo esses pesos em novas partículas.
\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.7\textwidth]{../../graphics/dethinning/dethinning_dmax-pt.png}
    \caption{Representação da geometria utilizada para reconstruir a informação do chuveiro por ``dethinning".}
    \label{fig:geometry_dethinning}
\end{figure}
A saída do simulador CORSIKA consiste de uma lista com informações de tipo, energia, momento, posição, ângulos e tempo de chegada das partículas ao solo. O processo de ``dethinnig" consiste em adicionar partículas a essa lista até que o peso estatístico $w$ da partícula se torne 1. Para se inserir novas partículas, deve-se seguir a geometria definida na figura \ref{fig:geometry_dethinning} e deve-se também executar o seguinte procedimento:

\begin{enumerate}
    \item Escolher um vértice aleatório sobre a trajetória da partícula com peso;
    \item Definir um cone centrado na trajetória da partícula com peso;
    \item Sortear um ponto dentro desse cone seguindo uma distribuição gaussiana em duas dimensões. Essa será a trajetória da nova partícula;
    \item Projetar a nova partícula até o nível do solo, adicionando-se o tempo e a energia;
    \item Realizar todos os passos $w-1$ vezes. Para o caso em que o peso $w$ não seja um número inteiro, adicionar aleatoriamente uma outra partícula, para a qual a probabilidade é dada pela parte decimal de $w$.
\end{enumerate}

Como nenhuma partícula pode chegar no solo antes da frente do chuveiro, teremos uma distância máxima $(D_{max})$ do solo para escolher o vértice. Essa escolha será definida pelo tempo de chegada da partícula ao solo e é dada por
\begin{equation}
    D_{max} = \frac{c^2 (t_i-t_0)^2 - | \vec{x_i} - \vec{x_0} |^2 }{ 2[c(t_i-t_0) - (\vec{x_i} - \vec{x_0})\cdot \hat{p_i}]},
    \label{eqn:distance_vertex}
\end{equation}
onde $\vec{x_i}$ e $\vec{x_0}$ é a posição da partícula com peso e a posição da primeira interação, respectivamente. Devemos enfatizar que $D_{max}$ é a separação máxima entre o vértice e o solo. Os parâmetros utilizados nesse processo são os seguintes:
\begin{enumerate}
    \item O ângulo do cone gaussiano é definido por $\beta d$ onde $d$ é a distância lateral do ponto de impacto do chuveiro em relação à partícula com peso, $\beta = 3\degree $/km para partículas eletromagnéticas e $\beta = 1\degree $/km  para múons e hádrons.
    \item A distribuição de energia para as novas partículas é proporcional a uma distribuição gaussiana centrada no valor de energia da partícula original com desvio padrão de $10\%$.
    \item Foi introduzida uma aceitação para as partículas seguindo a probabilidade $P = e^{\Delta \chi/\epsilon}$, onde $\Delta \chi$ é a diferença do comprimento das trajetórias da partícula nova em relação à partícula com peso e $\epsilon = 50$ g/cm$^2$.
    \item Os múons são criados mais tarde no desenvolvimento do chuveiro e, como consequência, a distância do vértice é menor que $D_{max}$, logo
    \begin{equation}
        D = | \vec{x_i} - \vec{x_0} | - X^{-1}( \vec{x_i}, \vec{x_0}, \alpha h).
        \label{eqn:distance_vertex_muon}
    \end{equation}
\end{enumerate}
Aplicamos esse procedimento a todos os chuveiros estudados nesse capítulo e o resultado é mostrado na figura \ref{fig:comparation_dethinning}. Aqui, temos a distribuição espacial dos múons que chegaram ao solo para um chuveiro simulado com o CORSIKA com energia de $10^{19}$ eV iniciado por um próton com ângulo zenital de $60\degree$ e azimutal $0\degree$.  A figura da esquerda é a simulação original, enquanto a figura da direita mostra o resultado após a aplicação do processo de ``dethinning" e a escala de cores indica a densidade de múons no local.
\begin{figure}[!htb]
    \centering
    \begin{subfigure}[b]{0.49\textwidth}
      \includegraphics[width=\textwidth]{../../graphics/muon_map/muon_map-eposlhc-proton-e19_00-t60.png}
      \caption{(``thinning'')}
	  \label{fig:thinning_comparation_dethinning}
    \end{subfigure}
    \begin{subfigure}[b]{0.49\textwidth}
      \includegraphics[width=\textwidth]{../../graphics/muon_map/de_muon_map-eposlhc-proton-e19_00-t60.png}
      \caption{(``dethinning'')}
	  \label{fig:dethinning_comparation_dethinning}
    \end{subfigure}
    \caption{Comparação da distribuição bidimensional de distribuição de partículas no solo para EAS iniciados por próton com energia de $10^{19}$ eV e inclinação zenital de $60\degree$ e azimutal $0\degree$. Em (a) temos o mapa original sem aplicação de ``dethinning" e em (b) temos o mapa após a aplicação do algoritmo de ``dethinning".}
    \label{fig:comparation_dethinning}
\end{figure}



