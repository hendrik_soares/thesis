Através dos tempos observados no processo de detecção, podemos determinar a direção de chegada do UHECR primário que iniciou o chuveiro. A direção pode ser reconstruída a partir dos tempos de chegada das partículas do chuveiro que cruzam os detectores, que se encontram na posições ($x_i$,$y_i$). Em primeira aproximação, podemos considerar a frente do chuveiro sendo plana e propagando-se à velocidade da luz $c$. Essa aproximação é válida para detectores com distância menor que $\sim 2000$ m do eixo do chuveiro. Os tempos de disparo dos detectores, para uma frente de chuveiro plana, podem ser descritos como 
\begin{equation}
    t_i = T_0 - \left[ \frac{u(x_i-x_c) + v(y_i-y_c)}{c} \right],
    \label{eqn:trigger_time_plane}
\end{equation}
onde $T_0$ é o instante em que o centro do chuveiro atinge a posição ($x_c,y_c$). As variáveis $u$ e $v$ são chamadas de cossenos diretores e são dadas por $u=\sin \theta \cos \phi$ e $v=\sin \theta \sin \phi$. Contudo, sabemos que a aproximação de frente do chuveiro plana não deve ser válida para detectores muito distantes do eixo do chuveiro. Dessa forma, devemos propor correções à equação \ref{eqn:trigger_time_plane} de modo a usar todos os detectores disponíveis. Como dados experimentais mostram que há uma curvatura na frente do chuveiro devido às características do desenvolvimento do mesmo, podemos corrigir a equação \ref{eqn:trigger_time_plane} com a adição de um termo $\Delta/c$. Por exemplo, para frente de chuveiros esférica ou parabólica devemos adicionar, respectivamente, ao lado direito de \ref{eqn:trigger_time_plane} um dos seguintes termos
\begin{equation}
    \frac{\Delta}{c} = \frac{(\sqrt{R^2+r_i^2}-R)}{c},  \qquad  \frac{\Delta}{c} \approx \frac{r_i^2}{2Rc},
    \label{eqn:curvature_term}
\end{equation}
onde $R$ é o raio de curvatura da frente do chuveiro e $r_i$ é a distância da posição do detector $r'_i$ projetada no plano do chuveiro. Para calcular o termo da curvatura $\Delta/c$, precisamos conhecer a distância do detector ($r_i$). Ou seja, dada a posição do detector ($x_i$,$y_i$) e as coordenadas polares da direção de entrada da cascata ($\theta$, $\phi$) podemos usar relações de geometria e determinar $r_i$ (Os detalhes para determinação de $r_i$ encontram-se no apêndice \ref{sec:distance_shower_plane}).

Para determinar a direção de chegada do EAS, faremos um processo de minimização de $\chi^2$ em duas etapas: uma primeira estimativa dos parâmetros será obtida por meio da aproximação de frente de chuveiro plana e, em seguida, essa estimativa inicial é utilizada para a determinação dos valores finais incluindo o efeito da curvatura. A estimativa inicial, envolve a minimização do $\chi^2$:
\begin{equation}
    \chi^2 = \sum^N_{i=1} \left( \frac{t^m_i - t_i}{\sigma_{i}} \right)^2,
    \label{eqn:chi_square_trigger_time}
\end{equation}
onde $t^m_i$ são os tempos medidos pela detecção com incertezas correspondentes $\sigma_i$ e $N$ é o número total de detectores atravessados por pelo menos uma partícula. O mínimo do $\chi^2$ deve corresponder a um ponto no espaço de parâmetros ($T_0$, $u$ e $v$) tal que $\partial \chi^2 / \partial T_0 = 0$, $\partial \chi^2 / \partial u = 0$ e $\partial \chi^2 / \partial v = 0$. Essas três equações podem ser postas na forma de uma única equação matricial da forma $Ax = b$ e esse sistema pode ser resolvido encontrando a inversa da matriz $3 \times 3$, de modo que $x = A^{-1} b$. Logo, cada inversão matricial irá fornecer o tempo $T_0$ e os cossenos diretores $u$ e $v$, e por consequência, as direções reconstruídas ($\theta_r$, $\phi_r$), já que 
\begin{equation}
    \theta_r = \text{asin} (\sqrt{u^2+v^2}) \quad \text{e} \quad \phi_r = \text{atan}(v/u).  
\end{equation}

Em seguida, devemos minimizar a equação da frente do chuveiro \ref{eqn:trigger_time_plane} com a adição do termo de curvatura \ref{eqn:curvature_term}. O mínimo do novo $\chi^2$ não leva mais a um sistema de equações lineares  e será necessário utilizar um método de minimização não linear para encontrar a solução. Neste caso, realizamos a minimização do $\chi^2$ com a biblioteca ``TMinuit'' fornecida pelo framework ROOT \cite{ROOTCERN} e utilizamos como valores iniciais os resultados obtidos através da aproximação de frente plana.

Por fim, podemos determinar a precisão da reconstrução através da resolução angular, evento a evento,  a partir das incertezas de $\theta$ e $\phi$ obtidos no processo de reconstrução, usando a relação:
\begin{equation}
    AR = 1.5 \sqrt{\frac{1}{2} \left( \sigma^2_{\theta} + \sin^2(\theta) \sigma^2_{\phi}  \right)}
	\label{eqn:angular_resolution}
\end{equation}
onde $\sigma_{\theta}$ e $\sigma_{\phi}$ são as incertezas de ajuste em $\theta$ e $\phi$ respectivamente\footnote{Essa relação foi extraída da referência \cite{BONIFAZI200920}.}.

