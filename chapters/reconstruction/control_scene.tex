Criamos um cenário para testar a reconstrução da direção de chegada e o parâmetro de tamanho do chuveiro $N_{19}$ a partir de dados extraídos da simulação do arranjo de detectores. Nesse cenário, utilizamos o mapa médio da densidade de múons para chuveiros iniciados por próton com energia de $10^{19}$ eV, com inclinação de $\theta = 60\degree$ e utilizando o modelo de interação hadrônica EPOSLHC, que é mostrado na figura \ref{fig:muon_map-eposlhc-proton-e19_00}. Para cada detector, localizado na posição ($x$,$y$), extraímos a densidade de múons e geramos flutuações estatísticas  através de uma distribuição de Poisson, dada por
\begin{equation}
    f(n;\lambda) = \frac{\lambda^n e^{-\lambda}}{n!}
\end{equation}
onde $\lambda = A_{det} \rho_{19}(x,y;\theta,\phi)$ é o número de múons dado pelo mapa de referência e $A_{det}$ é a área do detector. Os tempos de disparos dos detectores foram gerados pela equação equação \ref{eqn:trigger_time_plane} adicionando o termo de curvatura parabólica  \ref{eqn:curvature_term} e as flutuações estatísticas foram geradas a partir de um modelo, publicado em \cite{BONIFAZI2008523}, em que a variância $V[T_s]$ no tempo de chegada da primeira partícula ao detector é dada por \footnote{Esse modelo trata as diferenças de tempo dos múons com respeito à frente de onda da cascata como um processo de Poisson, bem como inclui às flutuações contribuições do detector devido à taxa finita de amostragem da eletrônica de aquisição. Os números adotados em \ref{eqn:variance_time_model} são consistentes com a eletrônica de aquisição do detector de superfície do Observatório Pierre Auger.} 
\begin{equation}
    V[T_s] = a^2 \left( \frac{2T_{50}}{n} \right)^2 \frac{n-1}{n+1} + b^2
	\label{eqn:variance_time_model}
\end{equation}
onde $a^2 = 1$, $b^2 = 144$ ns$^2$, $n$ é o número de partículas observado pelo detector e $T_{50}$ é tempo necessário para atingir 50\% do sinal do detector. 

Para esse teste, geramos 1000 simulações e reconstruímos a direção de chegada com o método descrito na seção \ref{sec:direction_arrive}. Essa reconstrução tem quatro parâmetros livres: o tempo do impacto no solo do centro do chuveiro ($T_0$), a inclinação do chuveiro em relação ao solo ($\theta$ e $\phi$) e a curvatura do chuveiro $R_c$. A figura \ref{fig:scene_t0_rec}, mostra a diferença do valor reconstruído ao original de $T_0$.
\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/reconstruct_scene/reconstruct_t0.pdf}
    \caption{ Reconstrução do parâmetro livre $T_0$ usando o método de minimização da frente de propagação curva.}
    \label{fig:scene_t0_rec}
\end{figure}
O valor de $T_0$ utilizado na simulação foi de 740 $\mu s$ e o reconstruído pelo método de minimização da frente do chuveiro $\langle T_0 \rangle = 740 \pm 200_{RMS} $ $\mu s$ onde o erro na média é de aproximadamente 200 ns.   No mesmo processo de reconstrução também determinamos os ângulos $\theta$ e $\phi$ do chuveiro, que são mostrados respectivamente nas figuras \ref{fig:scene_th_rec} e \ref{fig:scene_th_rec}. Como mencionado anteriormente, a inclinação dos chuveiros utilizados no teste era de $\theta = 60\degree$ e $\phi = 0 \degree$ e no processo de reconstrução encontramos o valor médio de $\langle \theta \rangle = 59.90 \pm 1.44_{RMS} \degree$ e  $\langle \phi \rangle = 0.02 \pm 1.42_{RMS} \degree$.  Esse resultados levam a uma resolução angular de $1.76\degree$ para o cenário de teste.
\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/reconstruct_scene/reconstruct_theta.pdf}
    \caption{ Reconstrução do parâmetro livre $\theta$ usando o método de minimização da frente de propagação curva.}
    \label{fig:scene_th_rec}
\end{figure}
\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/reconstruct_scene/reconstruct_phi.pdf}
    \caption{ Reconstrução do parâmetro livre $\phi$ usando o método de minimização da frente de propagação curva.}
    \label{fig:scene_phi_rec}
\end{figure}
Além dos parâmetros $T_0$, $\theta$ e $\phi$, a reconstrução também estima a curvatura da frente do chuveiro, $R_c$. Utilizamos o valor de $R_c = 10000$ m na simulação do cenário de teste e a reconstrução encontrou $\langle R_c \rangle = 10540 \pm 1328_{RMS}$ m. A curvatura do chuveiro foi próxima do valor esperado, porém entre todos os parâmetros é para o qual obtivemos mais flutuações durante a reconstrução da direção de chegada.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/reconstruct_scene/reconstruct_rc.pdf}
    \caption{ Reconstrução do parâmetro livre $R_c$ usando o método de minimização da frente de propagação curva.}
    \label{fig:scene_t0_rec}
\end{figure}

Como mencionado anteriormente, $N_{19}$ é reconstruído normalizando um mapa de densidade de múons, dado por $\rho_{19} (x,y;\theta,\phi)$, dependente da direção de chegada. Ou seja, dada a direção de chegada, podemos reconstruir o número de múons através do método de máxima verossimilhança usando uma distribuição de Poisson como estimador para a flutuação. A reconstrução de $N_{19}$, mostrada na figura \ref{fig:scene_n19_rec}, encontrou $\langle N_{19} \rangle = 1.00 \pm 0.10$, valor consistente com o esperado $N_{19} = 1$.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/reconstruct_scene/reconstruct_n19.pdf}
    \caption{ Reconstrução do parâmetro livre $N_{19}$ usando o método máxima verossimilhança. }
    \label{fig:scene_n19_rec}
\end{figure}

Sabemos que a reconstrução nunca será exata, pois reconstruímos com uma informação limitada à área de detecção. Para melhorar a reconstrução seria necessário aumentar a área de coleção através do aumento da área dos detectores, ou diminuir o espaçamento entre as células de detecção. Por exemplo, na figura \ref{fig:scene_n19_rec_size}, é mostrado a reconstrução do parâmetro $N_{19}$ para diferentes distâncias entre os detectores. Em verde, para os detectores separados por uma distância de 1500 m, a reconstrução de $N_{19}$ tem uma resolução de aproximadamente $0.1$. Quando diminuímos a distância, melhoramos a resolução para $0.05$ e $0.02$ para distâncias de 750 m (linha vermelha) e 375 m (linha azul), respectivamente. O mesmo comportamento é observado quando aumentamos a área de coleção dos detectores. Para $A_{det} = 10.18$ m$^2$, em azul, a reconstrução obteve uma resolução de aproximadamente $0.2$, que diminui para $0.05$ quando usamos detectores com $A_{det} = 40.71$ m$^2$, em vermelho, e por fim, obtivemos uma resolução de aproximadamente $0.03$ para detectores com $A_{det} = 90.61$ m$^2$, esses resultados encontram-se na tabela \ref{tab:n19_scene_recontruct}.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/reconstruct_scene/reconstruc_n19_size.pdf}
    \caption{ Reconstrução do parâmetro livre $N_{19}$ para diferentes espaçamento dos detectores. A linha azul representa a reconstrução de $N_{19}$ para detectores separados por 375 m, a linha vermelha para detectores separados por 750 m e a linha verde para detectores separados por 1500 m igual separação encontrada no Observatório Pierre Auger. }
    \label{fig:scene_n19_rec_size}
\end{figure}
\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.8\textwidth]{../../graphics/reconstruct_scene/reconstruc_n19_radius.pdf}
    \caption{ Reconstrução do parâmetro livre $N_{19}$ para diferentes área de exposição dos detectores.  A linha verde representa a reconstrução de $N_{19}$ para detectores com área de 90.61 m$^2$, a linha vermelha com área de 40.71 m$^2$ e a linha azul com área de 10.18 m$^{2}$ igual a área do detectores encontrado no Observatório Pierre Auger. }
    \label{fig:scene_n19_rec_radius}
\end{figure}
 
\input{../../data/reconstruct_scene/table.tex}

Os testes realizados aqui, num cenário controlado, validam a implementação do algoritmo de ``dethinning'' utilizado. Podemos então prosseguir e aplicar o algoritmo, assim como o processo de reconstrução, sobre cascatas geradas pelo programa CORSIKA.

