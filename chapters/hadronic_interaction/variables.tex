As variáveis comumente utilizadas no estudo de produção de hádrons são a rapidez ($y$), a pseudorapidez ($\eta$) e a variável de Feynman ($x_{F}$). Além disso, a rapidez tem relações úteis que a conectam às componentes do momento $\vec{p}$. Entretanto, a pseudorapidez é mais utilizada em experimentos de colisões de partículas, pois para determiná-la somente é necessário conhecer o ângulo da partícula em relação ao feixe de colisão. Já a variável de Feynman é muito útil para descrever colisões difrativas.

\subsection{Rapidez}

A rapidez $y$ de uma partícula viajando ao longo do eixo $z$ é uma quantidade adimensional, podendo ser positiva ou negativa, e é dada pela seguinte equação
\begin{equation}
  y = \frac{1}{2} \ln \left( \frac{E+p_z}{E-p_z} \right),
  \label{eqn:rapidity}
\end{equation}
onde está definida em termos da energia $E$ e $p_z$ é o momento longitudinal da partícula.  A rapidez, no limite não-relativístico, coincide com a velocidade da partícula em unidades da velocidade da luz. Por exemplo, no regime relativístico, a energia de uma partícula no sistema natural de unidades é dada por
\begin{equation}
  E= \gamma m
  \label{eqn:relativistic_energy},
\end{equation}
onde $m$ é a massa de repouso da partícula. O momento longitudinal é dado por
\begin{equation}
  p_z = \gamma \beta m.
  \label{eqn:relativistic_momentum}.
\end{equation}
Substituindo o $p_z$ e $E$ na equação \ref{eqn:rapidity} encontramos a rapidez de uma partícula viajando com velocidade $\beta$ na direção $z$ positiva
\begin{equation}
  y = \frac{1}{2} \ln \left( \frac{1+\beta}{1-\beta} \right).
  \label{eqn:rapidity_z_direction}
\end{equation}
No regime não relativístico, sabemos que $\beta \ll 1$ e a equação da rapidez torna-se
\begin{equation}
  y = \beta + O(\beta^3),
  \label{eqn:rapidity_expansion_z}
\end{equation}
que é aproximadamente a velocidade da partícula não relativística em unidade da velocidade da luz. 

Para mudar de um referencial $S'$ para $S$, com velocidade relativa $\beta$, aplicamos a transformação de Lorentz no quadrivetor energia-momento ($E'$, $\vec{p}' $) dado por
\begin{equation}
  E^{'} = \gamma (E-\beta p_z), ~
  p^{'}_z = \gamma (p_z-\beta E),
  \label{eqn:lorentz_boost_momentum}
\end{equation}
sobre a rapidez $y'$, dada no referencial $S'$ por
\begin{equation}
  y' = \frac{1}{2} \ln \left( \frac{{E'+p'}_z}{E'-p'_z} \right).
  \label{eqn:rapidity_frame_sp}
\end{equation}
Então, substituindo a equação \ref{eqn:lorentz_boost_momentum} em \ref{eqn:rapidity_frame_sp} temos
\begin{equation}
  y^{'} = y - \frac{1}{2} \ln \left( \frac{1+\beta}{1-\beta} \right).
  \label{eqn:rapidity_frame_sp_2}
\end{equation}
onde $y$ é a rapidez da partícula no referencial $S$. Expressando a rapidez neste formato podemos ver claramente que a transformação de Lorentz se reduz à soma de uma constante.

Para qualquer partícula livre podemos encontrar uma transformação do quadrivetor energia-momento ($E$, $\vec{p}$) para o espaço da rapidez e momento transverso ($y$, $\vec{p}_T$). Exponenciando a equação \ref{eqn:rapidity}, temos duas novas equações
\begin{equation}
  e^{y} = \sqrt{\frac{E+p_z}{E-p_z}},
  \label{eqn:exp_rapidity_pos}
\end{equation}
e
\begin{equation}
  e^{-y} = \sqrt{\frac{E-p_z}{E+p_z}}.
  \label{eqn:exp_rapidity_neg}
\end{equation}
A relação entre energia $E$ e rapidez $y$ da partícula pode ser encontrada somando as equações \ref{eqn:exp_rapidity_pos} e \ref{eqn:exp_rapidity_neg},
\begin{equation}
  E = m_{T} \cosh y,
  \label{eqn:relation_energy_y}
\end{equation}
onde $m_{T}$ é a massa transversa da partícula
\begin{equation}
  m^{2}_{T} = m^2 + \vec{p}^{2}_{T}.
  \label{eqn:tranverse_mass}
\end{equation}
Subtraindo as equações \ref{eqn:exp_rapidity_neg} e \ref{eqn:exp_rapidity_pos}, obtemos a relação entre o momento longitudinal $p_z$ e a rapidez da partícula
\begin{equation}
  p_z = m_{T} \sinh y.
  \label{eqn:relation_momentumz_y}
\end{equation}

A rapidez $y$ é uma escolha apropriada para descrever a dinâmica de partículas relativísticas,  pois se transforma de maneira simples sob {\it boosts} de Lorentz e as equações \ref{eqn:relation_energy_y} e \ref{eqn:relation_momentumz_y} são relações úteis que conectam as componentes do momento com a variável rapidez.

\subsection{Pseudorapidez}

A pseudorapidez $\eta$ é uma variável de fácil acesso experimental e podemos determiná-la conhecendo apenas o ângulo entre a partícula e o feixe pela relação
\begin{equation}
  \eta = - \ln[\tan(\theta/2)],
  \label{eqn:pseudorapidity_ang}
\end{equation}
onde $\theta$ é o ângulo entre a partícula com momento $\vec{p}$ e o eixo do feixe (tomado aqui como o eixo $z$ do sistema de coordenadas), como mostrado na figura \ref{fig:theta_and_eta}.
\begin{figure}[htb]
	\centering
	\includegraphics[width=0.4\textwidth]{chapters/hadronic_interaction/images/pseudorapidity.png}
	\caption{Gráfico mostrando a relação entre o ângulo da partícula em relação ao feixe e a variável $\eta$.}
	\label{fig:theta_and_eta}
\end{figure}
Em termos do momento, a pseudorapidez pode ser escrita como
\begin{equation}
  \eta = \frac{1}{2} \ln \left( \frac{|\vec{p}| + p_z}{|\vec{p}|-p_z} \right).
  \label{eqn:pseudorapidity_p}
\end{equation}
Comparando as equações \ref{eqn:rapidity} e \ref{eqn:pseudorapidity_p}, podemos ver que a pseudorapidez coincide com a rapidez quando o momento é grande, ou seja, quando $|\vec{p}| \approx E$.

Tal como na rapidez, podemos encontrar as equações que levam do espaço de momento ($E$, $\vec{p}$) para representação de pseudorapidez e momento transverso ($\eta$,$p_T$) e vice-versa. A partir da equação \ref{eqn:pseudorapidity_p}, podemos expressar $\eta$ em função do momento $\vec{p}$ da seguinte forma
\begin{equation}
  e^{\eta} = \sqrt{\frac{|\vec{p}|+p_z}{|\vec{p}|-p_z}},
  \label{eqn:exp_pseudorapidity_pos}
\end{equation}
e
\begin{equation}
  e^{-\eta} = \sqrt{\frac{|\vec{p}|-p_z}{|\vec{p}|+p_z}}.
  \label{eqn:exp_pseudorapidity_neg}
\end{equation}
Adicionando as equações \ref{eqn:exp_pseudorapidity_pos} e \ref{eqn:exp_pseudorapidity_neg}, obtemos a relação para o momento total
\begin{equation}
  |\vec{p}| = p_T \cosh \eta,
  \label{eqn:momento_to_eta}
\end{equation}
onde $p_T$ é a magnitude do momento transverso, dado por
\begin{equation}
  p_T = \sqrt{p^2 -p_z^2}.
  \label{eqn:transverse_momentum_mag}
\end{equation}
Subtraindo a equação \ref{eqn:exp_pseudorapidity_neg} de \ref{eqn:exp_pseudorapidity_pos}, obtemos a magnitude do momento longitudinal em função da pseudorapidez
\begin{equation}
  p_z = p_T \sinh \eta.
  \label{eqn:pz_from_eta}
\end{equation}
Também podemos expressar a rapidez $y$ em função de $\eta$ como
\begin{equation}
  y = \frac{1}{2} \ln \left[ \frac{\sqrt{p_T^2 \cosh^2 \eta + m^2} + p_T \sinh \eta}{\sqrt{p_T^2 \cosh^2 \eta + m^2} - p_T \sinh \eta} \right],
  \label{eqn:y_from_eta}
\end{equation}
onde $m$ é a massa de repouso da partícula. Analogamente, a pseudorapidez $\eta$ pode ser escrita em função da rapidez $y$ por
\begin{equation}
  \eta = \frac{1}{2} \ln \left[ \frac{\sqrt{m_T^2 \cosh^2 y + m^2} - m_T \sinh y}{\sqrt{m_T^2 \cosh^2 y - m^2} - m_T \sinh y} \right]
  \label{eqn:eta_from_y}
\end{equation}
e se a partícula tem distribuição $d^3N/dyd\vec{p}_T$ em termos da rapidez $y$, então a distribuição em pseudorapidez $\eta$ é dada por
\begin{equation}
  \frac{d^3N}{d\eta d\vec{p}_T} = \sqrt{1-\frac{m^2}{m^2_T \cosh^2 y}} \frac{d^3N}{dy d\vec{p}_T}.
  \label{eqn:dn_deta_from_dn_dy}
\end{equation}

Assim como na rapidez, a pseudorapidez é uma escolha apropriada para estudar experimentos de partículas relativísticas e ainda existem relações simples que a conectam com a rapidez.

\subsection{A variável de Feynman $x_F$}

A variável de Feynman $x_F$ é uma variável de escala definida para descrever a produção de hádrons em colisões núcleon-núcleon. Em processos de colisões inclusivos como $p + p \rightarrow A + X$, em que a colisão próton-próton produz uma partícula $A$ e partículas adicionais, denotadas simplesmente por $X$, podemos definir a variável de Feynman $x_F$ como
\begin{equation}
	x_F = \frac{P^A_z}{P^A_{z,max}},
\end{equation}
onde $P^A_z$ é o momento longitudinal da partícula $A$ no referencial de centro de massa e ${P^A}_{z,max}$ é o máximo de momento que a partícula $A$ poderia carregar, baseado na energia da colisão próton-próton. Claramente $x_F$ varia entre $-1$ e $1$ e é um invariante de Lorentz. No cenário de pártons, a variável $x_F$ está relacionada com a fração de momento carregada pelos pártons $x_i$ que interagiram durante o processo de colisões inclusivas e produziram a partícula $A$. Nesse cenário, $x_F$ é definida como
\begin{equation}
	x_F = \frac{P^A_z}{P^A_{z,max}} = x_1 - x_2,
\end{equation}
onde $x_1$ e $x_2$ é a fração de momento dos pártons envolvidos na produção de $A$.
