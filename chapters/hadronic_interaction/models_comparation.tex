Através dos modelos de interações hadrônicas podemos estudar qualitativamente a dependência dos observáveis de EAS em função dos parâmetros, como seção de choque, elasticidade, multiplicidade e razão de carga. Até o funcionamento do LHC, os modelos de interações hadrônicas não tinham restrições bem definidas para as energias de UHECR. As previsões dos modelos eram relativamente diferentes dependendo da extrapolação realizada \cite{Engel:2011zzb}. Atualmente, as extrapolações são menos divergentes para as energias de UHECR devido aos dados do LHC criarem restrições para a energia de $10^{17}$ eV em colisões próton-próton \cite{DENTERRIA201198}.

A seção de choque está fortemente relacionada com a profundidade máxima do chuveiro, $X_{max}$, principalmente porque ela define o livre caminho médio da interação. Dessa forma, a seção de choque inelástica é muito importante para o desenvolvimento do chuveiro. Por exemplo, a seção de choque inelástica para colisões próton-próton é usada como parâmetro para os modelos de interações hadrônicas. Atualmente, esse parâmetro é bem descrito por todos os principais modelos até as energias do LHC, como podemos ver na figura \ref{fig:cross_section_lhc_models}. Entretanto, vemos que as previsões para a seção de choque inelástica de colisões próton-ar têm diferenças significativas. Esse resultado geralmente é associado à falta de dados experimentais para colisões hádron-núcleo, e além disso, os dados que existem são para colisões com energias menores que as colisões próton-próton. Consequentemente, o desenvolvimento de EAS para diferentes seções de choque próton-ar pode levar a diferentes interpretações dos observáveis de EAS.

\begin{figure}[htb]
	\centering
	\includegraphics[width=1.0\textwidth]{chapters/hadronic_interaction/images/cross_section_lhc_models.png}
	\caption{Seção de choque inelástica para colisões próton-próton (esquerda), colisão próton-ar (linhas grossas à direita) e colisões píons-ar (linhas finas à direita). Na figura, o modelo QGSJETII é representado por linha tracejada vermelha, EPOSLHC por linha tracejada azul e o SIBYLL2.3  por linha tracejada verde. Figura extraída de \cite{pierog2017air}.}
	\label{fig:cross_section_lhc_models}
\end{figure}

A multiplicidade atua de maneira similar à seção de choque inelástica, porém com uma intensidade menor (veja seção \ref{sec:mattews_model}). Por outro lado, a predição dos modelos para a multiplicidade é relativamente bem diferente e insere mais incertezas sistemáticas nas medidas do que a seção de choque. Um exemplo é a distribuição de pseudorapidez $dN/d\eta$ para colisões próton-próton provenientes dos modelos, mostrada na figura \ref{fig:eta_distribution_lhc_models}. Os modelos são divergentes e o mais próximo dos dados do LHC é EPOSLHC, enquanto QGSJETII e SIBYLL2.3 não reproduzem os dados com a mesma precisão. Esse cenário torna-se pior quando olhamos a distribuição para colisões próton-ar, mostrado na figura \ref{fig:multplicity_energy_lhc_models} pois as diferenças tornam-se ainda maiores à medida que a energia aumenta. Assim como a seção de choque, mudanças na multiplicidade influenciam no desenvolvimento do EAS.

\begin{figure}[htb]
	\centering
	\includegraphics[width=1.0\textwidth]{chapters/hadronic_interaction/images/eta_distribution_lhc_models.png}
	\caption{Distribuição de pseudorapidez $dN/d\eta$ para eventos com ao menos uma partícula com $|\eta|<1$ (esquerda) e a  distribuição correspondente de multiplicidade (direita) para interações próton-próton com energia 7 TeV. Na figura, os resultados  do modelo QGSJETII são representado por linha tracejada vermelha, EPOSLHC por linha tracejada azul e o SIBYLL2.3  por linha tracejada verde. Figura extraída de \cite{pierog2017air}.}
	\label{fig:eta_distribution_lhc_models}
\end{figure}

\begin{figure}[htb]
	\centering
	\includegraphics[width=1.0\textwidth]{chapters/hadronic_interaction/images/multplicity_energy_lhc_models.png}
	\caption{ Multiplicidade em função da energia para eventos próton-próton (esquerda) e próton-ar a direita. Na figura, o modelo QGSJETII é representado por linha tracejada vermelha, EPOSLHC por linha tracejada azul e o SIBYLL2.3  por linha tracejada verde. Figura extraída de \cite{pierog2017air}.}
	\label{fig:multplicity_energy_lhc_models}
\end{figure}

Outro observável importante no desenvolvimento de EAS é a fração de energia carregada por ``uma partícula secundária''  chamada de partícula líder. Essa fração é chamada de elasticidade, e define a energia que não foi perdida durante o processo de colisão. Devido ao difícil acesso experimental à região frontal do espaço de fase do processo de colisão em experimentos com colisores, há poucos vínculos experimentais sobre a elasticidade nessa região em comparação com a região central,. Esse resultado gera comportamentos opostos nos modelos, por exemplo, o SIBYLL2.3 tem a maior elasticidade quando comparado ao EPOSLHC e QGSJETII, como mostrado na figura \ref{fig:elasticity_lhc_model}. Esse resultado está relacionado com o fato de ele ter a menor multiplicidade, logo a partícula líder perde menos energia, em média, durante o processo de colisão.

\begin{figure}[htb]
    \centering
		\includegraphics[width=1.0\textwidth]{chapters/hadronic_interaction/images/elasticity_lhc_models.png}
    \caption{Elasticidade (fração de energia da partícula líder) para colisões próton-próton (a esquerda) e colisões píon-ar (linhas finas na figura a direita) e a inelasticidade  próton-ar (linhas fortes na figura a esquerda) em função da energia de centro demassa . Simulações foram feitas com EPOSLHC(azul), DPMJETIII, (roxo) QGSJETII-04 (vermelho) e SIBYLL2.3 (verde). Figura extraída de \cite{pierog2017air}.}
    \label{fig:elasticity_lhc_model}
\end{figure}

Por sua vez, resultados obtidos pela Colaboração Auger encontraram uma diferença entre o número de múons produzidos na simulação em relação ao observado experimentalmente\cite{PhysRevD.91.032003}. Essa diferença pode ser atribuída aos modelos de interações hadrônicas, pois os múons são em grande parte produzidos pelos decaimentos de píons e káons gerados na componente hadrônica do chuveiro. Diferentes trabalhos já tentaram explicar essa diferença. Por exemplo, em \cite{PhysRevLett.101.171101} a contribuição da produção de bárions/antibárions, assim como a produção de $\rho^0$ ao longo da cascata foi estudada no tocante à produção final de múons. Outro trabalho mais recente \cite{Aduszkiewicz2017} mostrou que a produção de $\rho^0$  em interações píon-carbono pode estar subestimada por um fator que contribuiria com aumento na produção de múons.

Alguns estudos recentes, como \cite{ulrich_artigo}, mostram a influência dos principais parâmetros de interações hadrônicas sobre o desenvolvimento dos EAS. Através de mudanças sistemáticas nos valores de tais variáveis, aferiu-se que os parâmetros mais usados para inferir a composição química são sensíveis a diferentes extrapolações. Na figura \ref{fig:sistematic_changes_ulrich_xmax} vemos que mudanças na seção de choque, multiplicidade, elasticidade e razão de carga podem mudar a profundidade do máximo do chuveiro, $X_{max}$, em cerca de 20 g/cm$^2$. Esse resultado levaria a outras interpretações sobre a composição química dos UHECR.

\begin{figure}[htb]
	\centering
	\includegraphics[width=1.0\textwidth]{chapters/hadronic_interaction/images/sistematic_changes_ulrich_xmax.png}
	\caption{Mudança do parâmetro $X_{max}$ dos EAS devido a modificações sistemáticas na seção de choque, multiplicidade, elasticidade e razão de carga por meio de uma constante multiplicativa $f_{19}$ aplicada aos valores de tais parâmetros em $E=10^{19}$ eV. Extraído da referência \cite{ulrich_artigo}. }
	\label{fig:sistematic_changes_ulrich_xmax}
\end{figure}

Outros estudos como \cite{master_hendrik}, tomando por base um modelo devido a
L. Portugal e T. Kodama \cite{portugal_gluon},  mostram que o efeito de saturação de pártons pode influenciar na evolução subsequente dos EAS. A altíssimas energias, a seção de choque inelástica deve ser dominada pela interação com pártons de baixo momento (regime pequeno $x$) levando a uma saturação e, consequentemente, a seção de choque inelástica tem um aumento mais rápido quando comparado ao modelo de Glauber. Na figura \ref{fig:gluon_saturation_mass_hendrik_master} é mostrado o número de massa médio em uma escala logarítmica como função da energia. Nota-se que, ao inserir o efeito de saturação de pártons (Gaussiana, Woods-Saxon)\footnote{ Nomes utilizados pelo autor para diferenciar os modelos gerados por uma distribuição de perfil Gaussiana, ou Woods-Saxon para os pártons dentro do núcleo.} na seção de choque resultou em uma redução do número de massa média observado nos dados do Observatório Pierre Auger indicando uma composição química mais leve.

\begin{figure}[htb]
	\centering
	\includegraphics[width=1.0\textwidth]{chapters/hadronic_interaction/images/gluon_saturation_mass_hendrik_master.pdf}
	\caption{Número de massa médio em uma escala logarítmica como função da energia. Em verde e azul, temos os resultados da composição para os dois modelos da saturação de glúons. Quando comparados ao resultado do modelo do Glauber (SIBYLL), vemos que a composição para o modelo de saturação de glúons é de partículas mais leves do que o SIBYLL2.3. Retirado da referência \cite{master_hendrik}. }
	\label{fig:gluon_saturation_mass_hendrik_master}
\end{figure}
