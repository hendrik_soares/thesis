Chuveiros hadrônicos são cascatas iniciadas por interações hadrônicas de um ou mais núcleons.  Na primeira interação, uma fração da energia do núcleon é transferida para os mésons e bárions secundários. O resto da energia permanece com o núcleon que irá interagir novamente após atravessar um comprimento de interação $\lambda_{air}$. Enquanto isso, alguns mésons secundários também podem  interagir produzindo a segunda geração de hádrons da cascata. Esse processo continua até que o núcleon tenha energia abaixo do limite mínimo para produção de hádrons.

Os principais processos que ocorrem durante o desenvolvimento da cascata hadrônica são:
\begin{enumerate}
  \item Produção de píons, com altas energias, devido à interação do hádron.
  \item Os hádrons secundários e píons carregados têm energia suficiente para continuar se multiplicando em sucessivas gerações até que a energia não seja suficiente para produção de píons.
  \item A perda de energia por ionização da atmosfera devido aos hádrons carregados;
  \item Os píons neutros $\pi^0$ têm tempo de vida muito curto ($1.78 \times 10^{-16}$ s) antes de decair em dois fótons $\gamma$ ($\pi^0 \rightarrow 2 \gamma$) e cada $\gamma$ inicia um chuveiro eletromagnético;
  \item Muitos dos píons carregados decaem em múons via as interações
\begin{equation}
	\pi^+ \rightarrow \mu^+ + \nu_{\mu},
\end{equation}
\begin{equation}
	\pi^- \rightarrow \mu^- + \overline{\nu}_{\mu},
\end{equation}
pois o tempo de vida média desses píons é de $2.603 \times 10^{-8}$ s.
  \item Devido aos múons não interagirem fortemente, eles atravessam a atmosfera perdendo pouca energia por efeito de ionização. Às vezes podem decair, pois tempo de vida média dos múons é $2.196 \times 10^{-6}$ s: 
\begin{equation}
	\mu^+ \rightarrow e^+ + \nu_e + \bar{\nu_{\mu}},
\end{equation}
\begin{equation}
	\mu^- \rightarrow e^- + \bar{\nu_e} + \nu_{\mu}.
\end{equation}
\end{enumerate}

Diferentemente dos chuveiros eletromagnéticos que podem ser descritos de maneira bem direta por uma única teoria (QED), a produção de partículas em interações hadrônicas é fortemente dependente da energia e diferentes mecanismos de interações tornam-se importantes para cada região de energia estudada. Porém ainda podemos utilizar o modelo de Heitler-Matthews \cite{heitler-mathews-model}, que será discutido mais à frente, para compreender as principais características do desenvolvimento de EAS.

%Em energias próximas a  $\sqrt{s} \sim 1 - 2$ GeV, a produção de partículas é dominada pelo decaimento de ressonâncias hadrônicas. Para energias intermediárias, até $\sqrt{s} \sim 100$ GeV, estamos na região de {\it scaling}, que é melhor descrita pela teoria de Regge. Enquanto altíssimas energias $\sqrt{s} > 100$ GeV, ocorre a produção de jatos partônicos, ou mini-jatos, que são eficientemente descritos em termos de pártons. Os valores de energia aqui apresentado são valores aproximados de quando essas novas interações tornam-se importante e devem ser considerados nos cálculos.

Em torno de $\sqrt{s} \sim 1 - 2$ GeV, a produção de partículas pode ser descrita por modelos isobáricos \cite{isobaric_model}. A amplitude das ondas parciais de todos os diagramas para a troca de uma única partícula ou fusão de partícula-ressonância, incluindo todas as possíveis excitações de hádrons, são adicionadas com pesos, e a fase relativa é determinada por simetria e comparações com medidas. Dois exemplos desses processos são mostrados na figura \ref{fig:two_process_isobaric}. Píons positivos interagem com prótons e formam uma ressonância $\Delta^{++}$ (1232) que decai na sequência em $\pi ^+$ e $p$. Situação similar ocorre no caso da absorção de fóton exceto pelos diferentes canais de decaimento.
\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.7\textwidth]{chapters/cosmic_rays/images/resonance_example.png}
	\caption{Produção de ressonância $\Delta^{++}$ (1232) em interações de píons (esquerda) e fótons (direita) com prótons. Retirado da referência \cite{gaisser_2016}.}
	\label{fig:two_process_isobaric}
\end{figure}

A produção de partículas cresce em função da energia até valores da ordem $\sqrt{s} \sim 100$ GeV. Esse crescimento é descrito satisfatoriamente, de forma fenomenológica, pela teoria de Regge, onde a amplitude de espalhamento é descrita como uma função analítica do momento angular. Em geral, nessa teoria o momento angular não é restrito a número inteiros e pode assumir valores complexos. Dessa forma, a amplitude é considerada uma função complexa que descreve um caminho no plano complexo chamado trajetória de Regge. Essa generalização descreve que as forças de interação forte são originárias da troca de composições de partículas, ou polos de Regge, como mostrado na figura \ref{fig:regge_polo_example}.
\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.25\textwidth]{chapters/cosmic_rays/images/reggeon_exchange.png}
	\caption{Descrição da interação inelástica $1+2 \rightarrow 3+4$ mediada por reggeon. Retirado da referência \cite{vicenzo-book}. }
	\label{fig:regge_polo_example}
\end{figure}
Diversos modelos, como {\it párton duplo} (DPM) \cite{dual_parton_model} e {\it corda de quark-gluon} (QGS) \cite{quark_gluon_string_model} foram desenvolvidos usando a teoria fenomenológica de Regge.

A produção de mini-jatos começa a ocorrer para energias maiores que $\sqrt{s} > 100$ GeV e é descrita com sucesso pela QCD perturbativa, a qual descreve a produção de partículas em termos de pártons livres. Nesse cenário, o hádron é visto como um mar de pártons que são continuamente gerados e reabsorvidos. Durante o processo de colisão hadrônica, a interação é considerada sobre uma configuração independente, como mostrado na figura \ref{fig:minijet_example}.
\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.6\textwidth]{chapters/cosmic_rays/images/minijet_example.png}
	\caption{Ilustração da interação intermediada por pártons para produção de mini-jatos.}
	\label{fig:minijet_example}
\end{figure}

