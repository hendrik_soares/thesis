Chuveiros atmosféricos extensos (EAS) são cascatas de partículas que se desenvolvem na atmosfera terrestre após a colisão de um UHECR com um núcleo de ar (composto principalmente de nitrogênio e oxigênio) a uma altura típica de 15 a 35 km, em relação ao nível do mar. O EAS é um sistema complexo de produção de múltiplas partículas que pode ser separado nas componentes eletromagnética, muônica e hadrônica, como mostrado na figura \ref{fig:shower_model}. A componente eletromagnética é basicamente composta de fótons, elétrons e pósitrons, enquanto os múons desenvolvem a componente muônica e os prótons, píons carregados e fragmentos nucleares compõem a componente hadrônica.
\begin{figure}[!htb]
        \centering
        \includegraphics[width=0.8\textwidth]{chapters/cosmic_rays/images/shower_particle.png}
        \caption{Esquema do desenvolvimento das três componentes de um chuveiro atmosférico extenso. }
        \label{fig:shower_model}
\end{figure}

O EAS inicia quando um raio cósmico com altíssima energia colide com um núcleo presente na atmosfera. Desta primeira colisão são produzidas partículas altamente energéticas que continuam a penetrar na atmosfera. Essas partículas interagem novamente na atmosfera, iniciando um processo em cascata de produção de partículas. Os hádrons produzidos na cascata formam a componente hadrônica e, ao atingir o solo terrestre, se espalham por poucos metros de diâmetro em relação ao eixo do EAS\footnote{O eixo do EAS é uma linha reta que o raio cósmico teria seguido até atingir o solo, caso não interagisse.}. A cada interação nuclear também são produzidos píons neutros, que carregam uma fração significativa da energia do primário, e decaem imediatamente em fótons. Os fótons de altas energias, na presença do campo eletrostático de núcleos da atmosfera, convertem-se em pares elétron-pósitron. Já os elétrons/pósitrons interagem por efeito de {\it bremsstrahlung} e irradiam fótons, produzindo um chuveiro eletromagnético. Os píons carregados $\pi^{\pm}$, além de colidirem com os núcleos, podem também decair em múons carregados $\mu^{\pm}$ originando o chuveiro muônico. O desenvolvimento lateral da cascata ocorre devido ao efeito acumulado de sucessivos espalhamentos. O número de partículas continua aumentando devido às sucessivas interações até atingir uma profundidade de máximo do chuveiro, onde a probabilidade da partícula ser absorvida por ionização ao atravessar a atmosfera passa a ser maior que a probabilidade de produzir novas partículas. 

O desenvolvimento dos EAS pode ser observado sob aspectos de diferentes métodos, já que, durante seu desenvolvimento, diversos tipos de radiação são observados, como a emissão de rádio, quando partículas carregadas são defletidas pelo campo magnético terrestre (emissão coerente geomagnética \cite{ARDOUIN2009192}) e também pelo excesso de elétrons na frente do chuveiro (efeito Askarian \cite{PRESCOTT1971}). A radiação de fluorescência é emitida quando partículas carregadas do chuveiro atravessam a atmosfera terrestre e excitam as moléculas, principalmente N$_2$, e parte dessa energia de excitação é emitida em forma de radiação isotrópica, no espectro visível e no ultravioleta. 

As medidas de desenvolvimento do EAS são feitas geralmente em função da profundidade atmosférica, que é dada por 
\begin{equation}
  X = \int^{\infty}_{h} \rho(h') dh'
  \label{eqn:x_slant}
\end{equation}
onde $\rho(h')$ é a densidade atmosférica em função da altura $h'$. Nota-se que a grandeza $X$ é uma medida de quantidade de matéria atravessada normalmente dada em g/cm$^2$ e a integração da variável $h'$ é realizada ao longo do caminho representado pelo eixo da cascata. A profundidade atmosférica na qual o EAS atinge o maior número de partículas é chamada de $X_{max}$ e é um parâmetro importante no estudo de UHECR.
\begin{figure}[!htb]
        \centering
        \includegraphics[width=0.5\textwidth]{chapters/cosmic_rays/images/longitudinal_profile.png}
        \caption{Esquema do desenvolvimento longitudinal de um EAS com destaque aos principais pontos importantes.}
        \label{fig:longitudinal_profile}
\end{figure}
Na figura \ref{fig:longitudinal_profile} é mostrado um esquema do desenvolvimento longitudinal do chuveiro destacando pontos importantes, como a profundidade da primeira interação, $X_{1}$, a profundidade do máximo do chuveiro $X_{max}$ e o nível de observação $X_{obs}$. Esse último parâmetro é geralmente associado ao solo onde é instalado o arranjo de detectores para observar o desenvolvimento lateral do EAS. O perfil lateral de desenvolvimento do EAS mostrado na figura \ref{fig:lateral_profile}, é caracterizado pela densidade de partículas que atravessam o nível de observação em função da distância do eixo do chuveiro $r_{core}$.
\begin{figure}[!htb]
        \centering
        \includegraphics[width=0.4\textwidth]{chapters/cosmic_rays/images/lateral_profile.png}
        \caption{Perfil de desenvolvimento lateral de um EAS. Exemplo de espalhamento lateral da densidade de partículas, $\rho(r_{core})$, em função da distância radial $r_{core}$ do eixo do chuveiro.}
        \label{fig:lateral_profile}
\end{figure}


