O modelo de Matthews \cite{heitler-mathews-model} para EAS descreve as interações hadrônicas de modo similar ao modelo de Heitler, como mostrado na figura \ref{fig:matthews_model}. Podemos imaginar um hádron interagindo após atravessar uma camada $\lambda_I$, produzindo $N_{ch}$ píons carregados e $0.5 N_{ch}$ de píons neutros. Os píons neutros decaem em fótons imediatamente, produzindo sub-chuveiros eletromagnéticos. Já os píons carregados viajam uma distância fixa e interagem produzindo uma nova geração de píons. A multiplicação continua até que a energia individual dos píons esteja abaixo da energia crítica $\xi^{\pi}_c \approx 20$ GeV, momento no qual a probabilidade do píon decair se torna maior que a probabilidade de interação. Todas as interações são perfeitamente inelásticas, com toda a energia indo para a produção de novos píons. A multiplicidade $N_{ch}$ de colisões píons-núcleo é muito similar à interação hadrônica de colisões próton-próton \cite{PhysRevD.66.033011}. A produção de partículas cresce muito lentamente com a energia de laboratório, $E^{1/5}$, em dados de colisões pp \cite{PhysRevD.66.010001}. Desta forma, o modelo de Matthews adota a constante $N_{ch}=10$, um valor aceitável para o fator de produção de píons com energia cinética em torno de 1 GeV a 10 TeV.


\begin{figure}[htb]
	\centering
	\includegraphics[width=0.7\textwidth]{chapters/cosmic_rays/images/matthews_model.png}
	\caption{Visão esquemática do chuveiro hadrônico.}
	\label{fig:matthews_model}
\end{figure}

Considerando um raio cósmico entrando na atmosfera com energia $E_0$, teremos após $n$ interações $N_{\pi} = (N_{ch})^n$ píons carregados. Assumindo divisão igual de energia durante a produção de partículas, esses píons carregam uma energia total de $(2/3)E_0$. O restante da energia irá para os chuveiros eletromagnéticos que são iniciados pelo decaimento de $\pi^{0}$. Logo, a energia por píon carregado após $n$ interações é dada por
\begin{equation}
  E_{\pi} = \frac{E_0}{(1.5N_{ch})^n},
  \label{eqn:matthews_energy_per_pion}
\end{equation}
e o número de interações $n_c$ necessárias para chegar na energia crítica é
\begin{equation}
  n_c = \frac{\ln (E_0/\xi^{\pi}_c)}{\ln (1.5 N_{ch})} = 0.85 \log_{10} (E_0/\xi^{\pi}_c).
  \label{eqn:matthews_nint}
\end{equation}
Nesse modelo, o número de múons é dado por $N_{\pi} = N_{\mu}$, e a energia total
\begin{equation}
  E_0 = \xi_{c} N_e + \xi^{\pi}_c N_{\mu} \approx 0.85 \text{ GeV } (N_e + 24 N_{\mu}),
  \label{eqn:matthews_energy}
\end{equation}
mostra a contribuição relativa por múons e elétrons. Podemos determinar o número total de múons usando a equação \ref{eqn:matthews_nint} da seguinte forma
\begin{equation}
  \ln N_{\mu} = n_c \ln N_{ch} = \beta \ln(E_0/\xi^{\pi}_c),
  \label{eqn:matthews_nmu}
\end{equation}
onde $\beta = \ln (N_{ch})/ \ln (1.5N_{ch}) = 0.85$. Já a profundidade de máximo desenvolvimento $X_{max}$ é dada por
\begin{equation}
  X_{max} = X_0 + \lambda_{rad} \ln(E_0/(3 N_{ch}\xi_c)),
  \label{eqn:matthews_xmax}
\end{equation}
onde $X_0 = \lambda_I \ln 2$ é a profundidade da primeira interação. Por último, a taxa de elongação para um chuveiro de próton é dada por
\begin{equation}
  \Lambda^p = \Lambda + \frac{d}{d \log_{10} E_0} (X_0 -\lambda_{rad}  \ln(3N_{ch}) ) = 58 \text{ g/cm}^2 \text{ por década}.
  \label{eqn:matthews_elongation_rate}
\end{equation}.

Um modelo básico de EAS, similar ao modelo de Heitler para cascatas eletromagnéticas, nos permite predizer vários parâmetros razoavelmente bem quando comparado a simulações e dados. A energia primária é proporcional a uma combinação do número de múons e elétrons. O peso relativo depende principalmente das características da escala de energia na qual as interações hadrônicas e eletromagnéticas param de ocorrer. O crescimento do número de múons é lento e proporcional $\sim E^{\beta}_0$. O expoente é dependente da fração de energia dada aos píons carregados em cada interação. A taxa de elongação para EAS está em  acordo com as simulações.
