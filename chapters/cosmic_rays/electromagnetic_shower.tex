A componente eletromagnética dos EAS é caracterizada pela produção de cascatas de partículas eletromagnéticas. Um chuveiro eletromagnético começa quando um elétron, pósitron ou fóton de alta energia interage com o meio, produzindo uma cascata. O desenvolvimento do chuveiro pode ser imaginado a partir de um fóton de altíssima energia entrando na atmosfera. Após uma distância viajada, ele desaparece pela produção de par elétron-pósitron e a energia do fóton é dividida entre o elétron e o pósitron. Por sua vez, o elétron e o pósitron viajam uma distância e radiam fótons por efeito de {\it bremsstrahlung}. Assim, sucessivamente, por efeito de produção de pares e {\it bremsstrahlung}, novas partículas vão sendo criadas durante o desenvolvimento do chuveiro. A energia vai sendo repartida entre as partículas geradas no chuveiro, e também sendo perdida por ionização, até que os elétrons, pósitrons e fótons atingem a energia crítica $E_{c}$. Abaixo dessa energia, os elétrons não radiam mais fótons com energia suficiente para produzirem pares elétron-pósitron e os fótons acabam sendo absorvidos por efeito fotoelétrico.

Os processos básicos que governam uma cascata eletromagnética são a produção de pares elétron-pósitron, a emissão de radiação de {\it bremsstrahlung} e a perda de energia devida à ionização do meio interagente por partículas eletromagnéticas. Os  diagramas de Feynman, de ordem mais baixa em teoria de perturbação para a produção de pares elétron-pósitron e do efeito {\it bremsstrahlung} são mostrados na figura \ref{fig:feynman_diagrams_electromagnetic}, e suas formulas são devido a Bethe e Heitler \cite{bethe-Heitler}.

\begin{figure}[htb]
  \centering
  \begin{subfigure}[b]{0.35\textwidth}
    \includegraphics[width=\textwidth]{chapters/cosmic_rays/images/bremsstrahlung.png}
    \caption{{\it Bremsstrahlung}}
  \end{subfigure}
  \hspace{3cm}
  \begin{subfigure}[b]{0.25\textwidth}
    \includegraphics[width=\textwidth]{chapters/cosmic_rays/images/pair-production.png}
    \caption{Produção de Pares}
  \end{subfigure}
  \caption{Representações, em diagrama de Feynman, das principais interações eletromagnéticas. Em (a) temos o efeito {\it bremsstrahlung}, e em (b) ocorre a produção de pares elétron-pósitron.}
  \label{fig:feynman_diagrams_electromagnetic}
\end{figure}

A probabilidade de um elétron com energia $E$ radiar um fóton com energia $W = v E$ ao atravessar uma camada $dt = dX/X_{rad}$ da atmosfera é dada por $\phi(E, v) dt dv$
\begin{equation}
	\phi (E, v) \xrightarrow[E \gg E_c]{}  \phi (v) = v + \frac{1-v}{v} \left(\frac{4}{3} + 2b \right),
	\label{bremss-prob}
\end{equation}
o comprimento de radiação de um elétron no ar, $X_{rad}$ é $37.1$ g/cm$^2$, e $b = (18 \times \ln [183/Z^{1/3}])^{-1}$. Então, a taxa de energia perdida por {\it bremsstrahlung} é
\begin{equation}
\frac{dE}{dX} = - \frac{1}{X_{rad}} \int^1_0 (vE)\phi(v)dv = - \frac{1}{X_{rad}} E \times (1+b) \approx - \frac{E}{X_{rad}}.
\end{equation}
A  probabilidade total por unidade de comprimento de radiação para {\it bremsstrahlung}, dada por
\begin{equation}
	 \int^1_0 \phi(v)dv,
\end{equation}
contém uma divergência logarítmica em $v \rightarrow  0 $. Essa divergência requer um cuidado especial com a distribuição \ref{bremss-prob} quando usada no cálculo de Monte Carlo. Um limite inferior precisa ser introduzido na equação para evitar a divergência no infravermelho. Basicamente, precisamos escolher um corte, tal que, $v_{min}E_0 \ll E_{int}$, onde $E_{int}$ é a menor energia de interesse no problema. 

A expressão correspondente para produção de pares é a probabilidade $\Psi(W,u)dtdu$ de um fóton produzir um par elétron-pósitron onde a energia do pósitron é $E=uW$,
\begin{equation}
	\Psi (E, u) \xrightarrow[E \gg E_c]{}   \Psi (u) = \frac{2}{3} - \frac{1}{2} + \left(\frac{4}{3} + 2b\right)\left(u-\frac{1}{2}\right)^2.
	\label{pair-prob}
\end{equation}
Diferente do caso do {\it bremsstrahlung}, onde existe uma divergência característica na região do infravermelho, a função de distribuição de probabilidade da produção de pares pode ser integrada para encontrar a probabilidade total por unidade de comprimento de radiação,
\begin{equation}
 	\int^1_0 \Psi(u)du = \frac{7}{9}-\frac{1}{3}b \approx \frac{7}{9}.
\end{equation}

Além desses processos, temos também a perda de energia por ionização que ocorre quando uma partícula carregada atravessa um meio material, neste caso a atmosfera. A partícula excita os elétrons dos núcleos atômicos, perdendo parte de sua energia por ionização. Essa perda de energia pode ser escrita como
\begin{equation}
	dE/dX = -\alpha(E) -E/X_{rad},
	\label{eqn:energy_loss_ionization}
\end{equation}
onde $\alpha(E)$ é a energia perdida por unidade de comprimento devido à ionização e $dE/dX$ é a energia perdida na atmosfera pela partícula ao atravessar uma distância $X$ até $X+dX$. Na referência \cite{longair-book}, a  equação de perda de energia por ionização para elétrons, com energia $\gamma mc^2$, é dada por
\begin{equation}
- \frac{dE}{dx} = \frac{e^4N_e}{8\pi \varepsilon^2_0 m_e v^2} \left[ \ln \frac{\gamma m_e v^2 E_{max}}{2 \overline{I}^2} - \left( \frac{2}{\gamma}-\frac{1}{\gamma^2} \right) \ln (2) + \frac{1}{\gamma^2} + \frac{1}{8} \left( 1 - \frac{1}{\gamma}
 \right)^2 \right]
	\label{ionization_loss}
 \end{equation}
onde $N_e$ é o número de elétrons, $\varepsilon_0$ é a constante de permissividade do vácuo, $\overline{I}$ é a energia média de excitação e $E_{max}$ é a energia máxima transferida em uma única colisão. A perda de energia por colisão, ou espalhamento Compton, pode ser desprezada na cascata eletromagnética, pois enquanto a cascata mantiver uma energia muito maior que a energia crítica $E_c$, estaremos em uma situação completa de blindagem.


