O fluxo de raios cósmicos acima de 100 TeV é extremamente raro e sua detecção direta, com balões ou satélites, é muito difícil. Porém, ao entrar na atmosfera, os UHECR produzem uma cascata de partículas secundárias que são detectadas pelos experimentos de EAS. Sendo assim, todos os dados de espectro de energia para UHECR vêm de experimentos de EAS. Empiricamente, podemos dizer que o fluxo de raios cósmicos $N(E)dE$ segue uma distribuição do tipo lei de potência da seguinte forma
\begin{equation}
	N(E)dE \propto E^{-\gamma}dE
	\label{eqn:power_law_spectrum}
\end{equation}
onde $E$ é a energia, $N(E)$ é o número de partículas com energia entre $E$ e $E+dE$ e $\gamma$ é chamado de índice espectral de fluxo. Dependendo do intervalo de energia dos raios cósmicos, podemos ter diferentes valores de índices espectrais.
\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.8\textwidth]{chapters/cosmic_rays/images/high_energy_spectrum.png}
	\caption{Compilação de fluxo de vários experimentos de raios cósmicos. O fluxo foi reescalado por $E^{2,5}$ para que as características se tornem claramente visíveis. Retirado da referência \cite{lhc_constraints}}
	\label{fig:high_energy_spectrum}
\end{figure}
Na compilação do fluxo de vários experimentos de raios cósmicos, mostrado na figura \ref{fig:high_energy_spectrum}, podemos ver claramente três mudanças no índice espectral $\gamma$:

\begin{enumerate}
	\item Uma mudança chamada de {\it segundo joelho} para energias em torno de $10^{17}$ eV;
	\item Outra mudança chamada {\it tornozelo} em aproximadamente  $10^{18.4}$ eV;
	\item E uma forte {\it supressão} do espectro de energia em $10^{19.5}$ eV.
\end{enumerate}

Variações no comportamento do fluxo de raios cósmicos podem ocorrer devido a mudanças no mecanismo de produção e aceleração, mudanças nas fontes astrofísicas com contribuição majoritária ao fluxo, novas interações no meio interestelar e evolução cosmológica. Por exemplo, nas energias acessíveis ao Observatório Pierre Auger  que é mostrado na  figura \ref{fig:high_energy_spectrum_auger}, as medidas de fluxo apresentam duas mudanças espectrais bem pronunciadas, o {\it tornozelo} em torno de $10^{18.4}$ eV e a forte {\it supressão} no fluxo em torno de $10^{19.5}$ eV \cite{auger-supression}, essa última provavelmente associada à interação ressonante dos UHECR com a radiação cósmica de fundo (CMB).

\begin{figure}[!htb]
	\centering
	\includegraphics[width=0.7\textwidth]{chapters/cosmic_rays/images/auger_spectrum.png}
	\caption{O espectro de energia obtido pelo OPA por combinação de medidas hibridas usando telescópio de fluorescência e detectores de superfície.  Retirado da referência \cite{auger-spectrum}.}
	\label{fig:high_energy_spectrum_auger}
\end{figure}

Resultados recentes publicados pelo experimento KASCADE-Grande \cite{kascade-results} mostram que a mudança no índice espectral observada em torno de $10^{17}$ eV no fluxo total de partículas, e comumente denominada de {\it segundo joelho}, é determinada por um {\it amolecimento} \footnote{espectro mole é aquele com índice espectral grande, ou seja fortemente atenuado.} da componente pesada (elementos mais pesados que He). Observa-se também, no mesmo conjunto de dados, um {\it endurecimento} \footnote{espectro duro é aquele com índice espectral pequeno, ou seja, o espectro se estende até altas energias.} da componente mais leve (H e He) na mesma região do espectro. Ambas as mudanças espectrais podem ser uma indicação da transição entre as componentes galáctica e extragaláctica dos UHECR \cite{aloisio-spectrum}.

No {\it tornozelo}, existem dois cenários possíveis, um cenário de composição mista e outro chamado {\it modelo dip}. No cenário de composição mista \cite{mix_composition_scenario} o  {\it tornozelo} é descrito como uma transição da componente galática para extragaláctica em torno de $10^{18}$ eV.  Nesse modelo, assume-se uma composição de partículas pesadas nas fontes astrofísicas e o processo de foto-desintegração nuclear por interação com CMB dá origem a uma composição mista na Terra, como mostrado na figura \ref{fig:mixed_composition}. No modelo do {\it dip} \cite{dip_model} supõe-se que o fluxo seja puramente de prótons e o {\it tornozelo} seria devido à perda de energia por produção de pares devido às sucessivas interações com o CMB e a transição extragaláctica ocorreria em torno de $10^{17}$ eV. O fator de modificação\footnote{Fator de modificação é a razão entre o espectro calculado com todas as perdas de energias e o espectro somente com perdas adiabáticas.} nesse cenário é mostrado na figura \ref{fig:dip_model} .
\begin{figure}[!htb]
	\centering
	\includegraphics[scale=0.4]{chapters/cosmic_rays/images/mixed_composition.png}
	\caption{Fator de modificação do espectro de energia para o cenário de composição mista, indicando que o {\it tornozelo} seria devido à transição de componente galáctica para extragaláctica. Retirado da referência \cite{mix_composition_scenario}.}
	\label{fig:mixed_composition}
\end{figure}

\begin{figure}[!htb]
	\centering
	\includegraphics[scale=0.45]{chapters/cosmic_rays/images/dip_model.png}
	\caption{Fator de modificação do espectro de energia para o cenário do modelo do {\it dip}, indicando que o {\it tornozelo} é devido à perda de energia por produção de par. Retirado da referência \cite{dip_model}.}
	\label{fig:dip_model}
\end{figure}

A {\it supressão} possivelmente ocorre devido à interação de raios cósmicos com a CMB, chamado de corte Greisen-Zatsepin-Kuzmin (GZK) \cite{gzk-effect-greisen,gzk-effect-zatsepin-kuzmin}. Nesse corte, um dos processos esperado é que um próton de altíssima energia interaja com o fóton do CMB $\gamma_{CMB}$, criando uma ressonância que consequetemente decai, através do processo de foto-produção de píons, em nêutron e píons carregados
\begin{equation}
	p + \gamma_{CMB} \rightarrow \pi^{+} + n,
	\label{eqn:photon_pion_production_cmb}
\end{equation}
ou em próton e píon neutro
\begin{equation}
	p + \gamma_{CMB} \rightarrow \pi^{0} + p.
	\label{eqn:photon_pion_production_cmb_2}
\end{equation}
A energia mínima do próton para que ocorra o efeito GZK pode ser facilmente calculada utilizando a física relativística e a conservação do quadrimomento. Aplicando-se a conservação do quadrimomento $P$ ao sistema antes e depois da interação, que é dada por
\begin{equation}
	(P_p + P_{\gamma})^2 = (P_n +P_{\pi})^2,
	\label{eqn:4_momentum_conservation_pg_n_pi}
\end{equation}
podemos determinar a energia mínima do próton fazendo as seguintes considerações: (i) o nêutron e píon são criados em repouso ($P_n = M_n^2 c^2$ e $P_{\pi} = M_{\pi}^2 c^2$), (ii) a colisão próton-fóton é frontal e em direções opostas maximizando a energia da interação e (iii) consideraremos que a energia térmica mais provável do fóton do CMB é  $E_{\gamma} = 3 \times k_B T = 7.89 \times 10^{-10}$ MeV. Resolvendo a equação de conservação  \ref{eqn:4_momentum_conservation_pg_n_pi} a menor energia do próton para esse processo é
\begin{equation}
	E_p = \frac{(M_n c^2 + M_{\pi}c^2)^2 - (M_pc^2)^2}{4E_{\gamma}},
\end{equation}
e substituindo alguns números como a massa do nêutron $M_n = 939.6$ MeV/c$^2$, a massa do próton $M_p = 938.3$ MeV/c$^2$ e a massa do píon $M_{\pi} = 139.6$ MeV/c$^2$, determinamos que a energia mínima
para que ocorra a interação ($p +\gamma_{CMB} \rightarrow n + \pi$) é
\begin{equation}
	E_p \approx 10^{20} ~ \textrm{eV}.
	\label{eqn:min_energy_p_gamma}
\end{equation}
Esse limiar do corte GZK é uma aproximação, pois existe um apreciável número de fótons do CMB mais energéticos na cauda da distribuição de Planck. Considerando esse detalhe, a energia de supressão GZK começaria em torno de $3 \times 10^{19}$ eV.

A produção de píons não é o único processo de interação esperado do próton com o CMB. Em particular, é esperado que também ocorra a produção de pares da seguinte forma
\begin{equation}
	p + \gamma_{CMB} \rightarrow e^+ + e^- + p.
	\label{eqn:pair_production_cmb}
\end{equation}
Esse processo pode ser tratado como perda contínua de energia, já que a energia do par $e^+/e^-$ criado é muito pequena (um infinitésimo) comparada à energia do próton. Então, espera-se que os prótons com altíssimas energias percam parte de sua energia ao se propagarem pelo meio interestelar, limitando a distância máxima da fonte para UHECR \cite{horizon_uhecr}.

No caso de núcleos em altíssimas energias, a situação se altera ligeiramente, pois, além do CMB, espera-se que o núcleo interaja também com a luz extragaláctica de fundo (EBL) \cite{PhysRevD.1.1596}. O núcleo propagando-se no meio interestelar pode interagir via produção de par e fotodesintegração, sendo este último um processo no qual o núcleo interage com o CMB ou EBL e perde um ou mais núcleons, da seguinte forma
\begin{equation}
	A + \gamma_{CMB,EBL} \rightarrow (A -nN) + nN,
	\label{eqn:photon_desintegration_cmb}
\end{equation}
ou seja, um núcleo de número de massa $A$ que foi emitido na fonte de raios cósmicos pode interagir com o CMB/EBL e perder um ou mais núcleons e, quando chegar na Terra, sua massa será descrita por $(A-nN)$. A fotodesintegração também pode ser responsável pela supressão no espectro observado de UHECR.
