O modelo semi empírico de Heitler \cite{heitler-model-book} é usado para descrever o desenvolvimento de chuveiros eletromagnéticos e prediz com precisão aceitável as propriedades mais importantes desses chuveiros. Esse modelo é composto de fótons e elétrons/pósitrons\footnote{No modelo de Heitler não há distinção entre elétrons e pósitrons} que repetidamente interagem e produzem duas partículas. Após viajar uma distância fixa $X$, definida pelo comprimento de radiação $\lambda_{rad}$, os elétrons interagem por {\it bremsstrahlung} e os fótons por produção de par elétron-pósitron. Depois de $n$ interações, haverá um total de $2^n$ partículas no chuveiro. A multiplicação é interrompida quando a energia individual de cada partícula estiver abaixo da energia crítica $\xi_c$. Nesse ponto a perda média de energia por colisão torna-se maior que a perda por radiação. Desse cenário, duas propriedades muito importante são bem reproduzidas: (i) o número final de partículas $N_{max}$ é simplesmente proporcional à energia inicial $E_0$ e (ii) a profundidade máxima do desenvolvimento do chuveiro é proporcional ao logarítmo de $E_0$.

\begin{figure}[hbt]
  \centering
  \includegraphics[width=0.5\textwidth]{chapters/cosmic_rays/images/heitler_model_shower.png}
  \caption{Visão esquemática do modelo de Heitler para chuveiros eletromagnético. O chuveiro inicia com uma partícula que viaja por uma distância fixa $X$, definida pelo comprimento de radiação $\lambda_{rad}$, e interage produzindo duas partículas. Esse processo se repete $n$ vezes até atingir o número máximo de partículas.}
  \label{fig:heitler_model}
\end{figure}

Como visto na figura \ref{fig:heitler_model}, um elétron, com energia $E_0$, radia um fóton após viajar um comprimento de radiação\footnote{Note que $d$ é de fato a distância em que o elétron perde, na média, metade de sua energia por radiação}
\begin{equation}
  d = \lambda_{rad}  \ln 2.
\end{equation}
e um fóton se desintegra em um par elétron-pósitron, ao viajar a mesma distância. Nesse instante, assume-se que a energia da partícula inicial (elétron ou fóton) é igualmente dividida entre as duas partículas finais. Então, após $n$ interações, a profundidade do chuveiro é
\begin{equation}
  X = n \lambda_{rad} \ln 2,
\end{equation}
e o tamanho total do chuveiro é dado por $N = 2^n =  e^{X/\lambda_{rad}}$. A multiplicação acaba quando a energia se torna menor que a energia crítica $\xi_c$. A energia crítica $\xi_c$ para chuveiros eletromagnéticos no ar é 85 MeV. Quando o chuveiro atinge o seu tamanho máximo $N=N_{max}$, todas as partículas terão a energia $\xi_c$ e a energia total $E_0$ pode ser descrita como
\begin{equation}
  E_0=\xi_c N_{max}
  \label{eqn:energy_heitler_model}.
\end{equation}
A profundidade de penetração máxima $X_{max}$, na qual o chuveiro atinge o seu tamanho máximo, é obtida pela determinação do número $n_c$ de divisões requerida para que a energia por partícula seja reduzida para $\xi_c$. Então, sabendo que $N_{max}=2^{n_c}$, obtemos da equação \ref{eqn:energy_heitler_model} que
\begin{equation}
  n_c = \ln \left( \frac{E_0}{\xi_c} \right),
  \label{eqn:nint_heitler_model}
\end{equation}
e, consequentemente,
\begin{equation}
  X_{max} = n_c \lambda_{rad} \ln 2 = \lambda_{rad} \ln \left( \frac{E_0}{\xi_c} \right).
  \label{eqn:xmax_heitler_model}
\end{equation}
A taxa de elongação $\Lambda$ é o crescimento de $X_{max}$ em relação ao logaritmo da energia $E_0$, definida como
\begin{equation}
  \Lambda \equiv \frac{dX_{max}}{d\log_{10}E_0}.
\end{equation}
Se usarmos $X_{max}$ da equação \ref{eqn:xmax_heitler_model} ,temos $\Lambda = 2.3 \lambda_{rad} = 85$ g/cm$^{2}$ por década de energia do primário para chuveiros eletromagnéticos no ar. A figura \ref{fig:electromagetic_shower_sim} mostra uma simulação detalhada de chuveiro eletromagnético no ferro ($\lambda_{rad}=13.8$ g/cm$^{2}$ e $\xi_c = 22.4$ MeV). A profundidade máxima, prevista pela equação \ref{eqn:xmax_heitler_model} de  $X_{max} = 7.4 \lambda_{rad}$, está muito próxima do valor da simulação.
\begin{figure}[hbt]
  \centering
  \includegraphics[width=0.6\textwidth]{chapters/cosmic_rays/images/electrogmagnetic_shower_sim.png}
  \caption{Simulação de chuveiro eletromagnético no meio Ferro. Os pontos são elétrons e fótons, e a curva mostra a energia depositada por comprimento de interação. Extraída da referência \cite{heitler-mathews-model}}
  \label{fig:electromagetic_shower_sim}
\end{figure}
O modelo superestima o número de fótons e elétrons: ele prediz que após algumas gerações, o número de elétrons $N_e \approx (2/3) N_{max}$. Esse valor é muito alto por diversas razões e a principal é que múltiplos fótons podem ser radiados durante emissão de {\it bremsstrahlung}. Simulações mostram que o número de fótons é muito maior que o número de elétrons durante o desenvolvimento do chuveiro, como visto na figura \ref{fig:electromagetic_shower_sim}. Para extrair o número correto de elétrons $N_e$ do modelo Heitler, temos que adotar um fator de correção:
\begin{equation}
  N_e = N/g,
  \label{eqn:correction_fator_heitler}
\end{equation}
onde o valor da constante $g=10$. Desconsiderando suas limitações, o modelo de Heitler consegue reproduzir o tamanho máximo do chuveiro, proporcional a $E_0$,  e a profundidade máxima que aumenta com o logaritmo da energia, com a taxa de 85 g/cm$^2$ por década de energia.
