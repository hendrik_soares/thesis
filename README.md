# Tese de Doutorado

## Sumário

* Introdução
* Raios cósmicos 
    - [ ] Introdução à raios cósmicos
    - [ ] Raios cósmicos de altissímas energias (UHECR)
    - [ ] Chuveiro atmosféricos extensos (EAS)
    - [ ] Observatório Pierre Auger (PAO)
    - [ ] Principais resultados de UHECR
* Interação hadrônicas
    - [ ] Interações hadrônicas de altíssimas energias 
    - [ ] Cinemática e variáveis
    - [ ] Geradores de eventos baseado em Monte Carlo
    - [ ] Diferenças entre os geradores de eventos
    - [ ] Geradores de eventos em medidas de EAS
* Simulação e análise de EAS
    - [ ] Simulação do conjunto de EAS
    - [ ] Análise do desenvolvimento longitudinal
    - [ ] Número de múons no nível de observação 
    - [ ] Profundidade máxima de produção de múons (MPD)
    - [ ] Distribuição lateral de múons (LDF)
* Reconstrução do número de múons **(TRABALHANDO NESSA SEÇÃO)**
    - Simulação de um conjunto de detectores
        1. [x] Descrição do que pretende simular
        1. [x] Layout do arranjo de detectores
        1. [ ] Funcionamento do detector 
        1. [ ] Geração de sinal sobre os detectores
        1. [x] Figura do examplo de sinal gerado
    - Determinando a direção de chegada
        1. [x] Descrição do modelo matemático
        1. [x] Método de analítico para frente plana
        1. [x] Método de minimização para frente curva 
    - Obtendo o número de múons 
        1. [x] Descrição do modelo matemático
        1. [x] Mostrar mapa de referência
        1. [x] Método de minimização
    - O problema do thinning em EAS
        1. [x] Thinning em EAS
        1. [x] Descrição do problema em medidas no solo
        1. [x] Dethinning em dados do EAS
        1. [x] Gráfico da densidade de múons antes e depois
    - Reconstrução sobre um cenário controlado
        1. [ ] Descrição do cenário controlado usando um mapa de referencia
        1. [ ] geração do número de múons
        1. [ ] Geração do tempo chegada
        1. [ ] Descrever a inserção de erros na densidade 
        1. [ ] Descrever a inserção de erros no tempo da frente do chuveiro
        1. [ ] Reconstrução do número de múons
        1. [ ] Diminuindo as incertezas de reconstrução
        1. [ ] Maior área de coleção 
        1. [ ] Diminuindo a distância entre os detectores
    - Reconstrução sobre simulaçoes de EAS
        1. [ ] Distribuição de reconstrução de $N_{19}$
        1. [ ] Diferença entre $N_{19}$ e $N_{\mu}$ 
    - Aprimorando o método de reconstrução
        1. [ ] Maior área de coleção 
        1. [ ] Diminuindo a distância entre os detectores
    - Evolução com a energia
        1. [ ] Obter as distribuições de $N_{19}$ para cada itnervalo de energia.
            - [ ] próton
            - [ ] ferro    
        1. [ ] Fazer o gráfico de $<N_{19}>$ em função da energia.
        1. [ ] Descrever os resultados de $<N_{19}>$ em função da energia.
* Distribuição de $dN/d\eta$ e EAS
    - [ ] $dN/d\eta$ na primeira interação de EAS
    - [ ] Importância de $dN/d\eta$ no desenvolvimento de EAS
    - [ ] Modelando a distribuição
    - [ ] Modificando a distribuição de partículas
    - [ ] Análisando os observáveis de EAS
* Anexo
    - [ ] Custo computacional


## Artigos 

* A phenomenological model of the muon density profile on the ground of very inclined air showers - **[1-s2.0-S0927650510001180-main.pdf]**
